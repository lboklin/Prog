extends Spatial
class_name Damaging
""" Base class for nodes which cause damage such as projectiles and hitscans """


signal hit(body)


export(bool) var damage_enabled := true
export(int, 0, 1000) var damage := 100


var belongs_to: Prog


var excluded_bodies: Array = [self, self.belongs_to] \
		setget set_excluded_bodies
func set_excluded_bodies(excs):
	for exc in excs:
		add_exception(exc)
	return


func _ready() -> void:
	return


# This is meant to be overriden by extending classes.
func add_exception(_exc: PhysicsBody) -> void:
#	breakpoint
	return


func _cause_damage(victim: PhysicsBody, step := 1.0) -> bool:
	var dmg := self.damage * step
	if self.damage_enabled \
			and not self.excluded_bodies.has(victim) \
			and victim and victim.has_method("take_damage") \
			and victim.take_damage(self.belongs_to.display_name, self.damage * step):
		print(victim.display_name, " was hit by ", self.belongs_to.display_name, "'s ", self.name, " for ", dmg)
		return true
	else:
		return false


func _process_hit(body: PhysicsBody, step := 1.0) -> bool:
	if _cause_damage(body, step):
		emit_signal("hit", body)
		return true
	else:
		return false