extends Spatial
class_name BeamWeapon

signal firing
signal deactivated
signal activated
signal cancelled(atk_pos)

export(NodePath) var prog_path: NodePath
onready var prog := get_node(prog_path) as Prog

# The side (body component) to control for aiming etc
export(NodePath) var side_path: NodePath
onready var side := get_node(side_path) as Side

# The side (body component) to control for aiming etc
export(Array, NodePath) var excluded_bodies = []

# How long the weapon can be fired before automatically deactivating
export(float) var max_firing_time := 0.4
export(float) var max_aiming_time := 0.8
# How long until the weapon can fire again
export(float) var cooldown := 0.8
# Unused for now, but works as a reminder to implement an energy system
#export(int) var power_consumption := 25 # Per activation

# Maximum reach of your attacks
export(float) var max_attack_range := 5.0
# Maximum angle of the sides' rotation around the up axis
#export(float) var max_sideways_angle := 45.0

var energy_beam: EnergyBeam

# Hue of energy (beam or otherwise)
var energy_hue := 0.0

# Counts down after deactivation and prevents another activation until 0
var cooldown_timer := 0.0
# Counts down while is_firing
var firing_timer := 0.0
var aiming_timer := 0.0

# True when aiming or firing
var is_active := false
# True when actively firing
var is_firing := false
# True when still aligning and not firing
var is_aiming := false

var attacking_pos := Vector3() setget set_attacking_pos
func set_attacking_pos(atk_pos) -> void:
	if atk_pos == Vector3():
		attacking_pos = atk_pos
	else:
		# Restrict range
		var wpn_pos: Vector3 = self.global_transform.origin
		var vec := Util.clamp_length(atk_pos - wpn_pos, 1.0, self.max_attack_range)
		attacking_pos = wpn_pos + vec
	self.side.face_pos = attacking_pos
	return


func _ready() -> void:
	self.side.max_look_range = self.max_attack_range
#	self.update_color(self.prog.secondary_color)
	self.prog.connect("colors_changed", self, "update_color")
	return


func _process(delta: float) -> void:
	if self.cooldown_timer > 0:
		self.cooldown_timer -= delta
#	elif self.firing_timer > 0: # Actively firing
	elif self.is_firing: # Actively firing
		self.firing_timer -= delta
		if self.side.is_blocked() or not self.side.is_aligned or self.firing_timer <= 0:
			deactivate()
#	elif self.aiming_timer > 0: # Actively aiming
	elif self.is_aiming: # Actively aiming
		print(aiming_timer)
		self.aiming_timer -= delta
		if self.aiming_timer <= 0:
			cancel_attack(true)
		elif self.attacking_pos != Vector3() and not self.is_firing: # Should be firing
			activate(self.attacking_pos)
	assert(is_active == (attacking_pos != Vector3()))
	return


remotesync func activate_side(atk_pos: Vector3) -> void:
	# Open or close side
	self.side.open_amount = self.side.max_open if atk_pos != Vector3() else 0.0
	return


func activate(atk_pos, ignore_alignment := false) -> bool:
	if self.side.is_blocked():
		if self.is_aiming:
			cancel_attack(false)
		elif self.is_firing:
			deactivate()
		return false

	elif self.is_firing or self.cooldown_timer > 0:
		if atk_pos == Vector3():
			deactivate()
		return false

	elif atk_pos != self.attacking_pos:
		self.attacking_pos = atk_pos
		rpc("activate_side", atk_pos)
		assert(self.attacking_pos != Vector3())
		assert(atk_pos != Vector3())

		if not self.is_active:
			emit_signal("activated")
			self.is_active = true
			self.is_aiming = true
			if self.aiming_timer <= 0:
				self.aiming_timer = self.max_aiming_time

	if ignore_alignment or self.side.is_aligned:
		rpc("fire")
		return true # Successfully fired
	else:
		rpc("aim", self.attacking_pos)
		return true # Successfully aiming


# Deactivate the beam
func deactivate() -> void:
	if not self.is_active:
		return

	activate_side(Vector3())

	self.cooldown_timer = self.cooldown
	self.firing_timer = 0.0
	self.is_active = false
	self.is_firing = false
	self.attacking_pos = Vector3()
	emit_signal("deactivated")

	var beam := self.energy_beam
	if is_instance_valid(beam) && beam is EnergyBeam:
		beam.expires_after = 0.0
		self.energy_beam = null
	return


func cancel_attack(abort_entirely := false) -> void:
	if (self.attacking_pos != Vector3()):
		var atk_pos := Vector3() if abort_entirely else self.attacking_pos
		deactivate()
		emit_signal("cancelled", atk_pos)
	self.is_aiming = false
	self.aiming_timer = 0.0
	assert(self.attacking_pos == Vector3())
	return


remotesync func aim(atk_pos: Vector3):
	activate_side(atk_pos)
	self.is_aiming = true
	return


remotesync func fire():
	var beam := preload("res://prog/weapon/EnergyBeam.tscn").instance() \
		as EnergyBeam
	beam.damage = 500
	beam.belongs_to = self.prog
	beam.excluded_bodies.push_front(self.side)
	beam.excluded_bodies.push_front(self.prog)
	for body_path in self.excluded_bodies:
		beam.excluded_bodies.push_front(get_node(body_path))
	beam.expires_after = self.max_firing_time
	beam.hue = self.energy_hue
	add_child(beam)

	self.energy_beam = beam
	self.is_aiming = false
	self.is_firing = true
	self.firing_timer = self.max_firing_time

	emit_signal("firing")
	return


func update_color():
	self.energy_hue = self.prog.secondary_color.h
	return