extends Damaging
class_name EnergyBeam


export(float, 0, 1) var hue := 0.75 setget ,get_hue
func get_hue() -> float:
	return self.mesh_instance.get("material/0").emission.h

export(float) var expires_after := 0.5
export(int, 0, 30) var max_impacts := 10


var _age := 0.0
var _impacts := 0


onready var impact = preload("res://prog/weapon/EnergyImpact.tscn")
onready var mesh_instance := $MeshInstance as MeshInstance
onready var spotlight := $SpotLight as SpotLight
onready var raycast := $RayCast as RayCast


#enum BeamColor {
#	BLUE,
#	CYAN,
#	GOLD,
#	GREEN,
#	LIME,
#	MAGENTA,
#	ORANGE,
#	PURPLE,
#	RED,
#	VIRIDIAN,
#}


func _init(dmg := 1000) -> void:
	self.damage = dmg # Per second
	return


func _ready() -> void:
	assert(is_instance_valid(belongs_to))
	var mat = self.mesh_instance.get("material/0").duplicate()
	self.mesh_instance.set("material/0", mat)
	set_hue(hue)
	return


func _on_impact_expired() -> void:
	self._impacts -= 1
	return


func _physics_process(delta: float) -> void:
	var cols := _detect_hit(delta)
#	if 0 < cols.size():
	for col in cols:
		if self.global_transform.origin.distance_to(col) > 1:
			new_impact(col)
#	elif is_instance_valid(self.impact):
#		var pos := self.global_transform.origin
##		var cyl := (self.mesh_instance.mesh as CylinderMesh)
##		var vec := -self.global_transform.basis.z * cyl.height
#		var end := pos + to_global(self.raycast.cast_to)
#		new_impact(end)
##		self.impact.sparkle(end)
##		self.impact.end_in(0.1)

	_age += delta
	if expires_after <= self._age:
		# Show ourselves for one more frame
		# to avoid invisible beam kills and such
		self.call_deferred("end")
	return


func set_hue(val: float) -> void:
	if is_instance_valid(self.spotlight):
		self.spotlight.light_color.h = val
	self.mesh_instance.get("material/0").emission.h = val
	hue = val
	return


func add_exception(exc: PhysicsBody) -> void:
	self.raycast.add_exception(exc)
	return


func new_impact(pos: Vector3) -> void:
	if self._impacts <= max_impacts:
		var imp: Area = self.impact.instance().duplicate()
		imp.belongs_to = self.belongs_to
		imp.excluded_bodies = self.excluded_bodies
		var imparent := get_tree().get_root()
		imparent.add_child(imp)
		imp.set_owner(imparent)
		imp.sparkle(pos)
		imp.end_in(0.1)
		imp.connect("tree_exited", self, "_on_impact_expired")
		self._impacts += 1
	else: # No more self._impacts allowed
		return


func end() -> void:
	self.queue_free()
	return


func _detect_hit(delta: float) -> PoolVector3Array:
	var collider: Spatial = self.raycast.get_collider()
	if collider:
		var col_pts := PoolVector3Array([self.raycast.get_collision_point()])
		if ._process_hit(collider, delta):
			# Run another collision check for objects behind this one since
			# a puny destructible object does not stop the beam
			self.add_exception(collider)
			self.raycast.force_raycast_update()
			col_pts.append_array(_detect_hit(delta))
		self.raycast.clear_exceptions() # Next tick we check all hits again
		return col_pts
	else:
		return PoolVector3Array()


#func beam_color(bc: int) -> Color:
#	match bc:
#		BeamColor.BLUE: return Color(0.572549, 0.729412, 1)
#		BeamColor.CYAN: return Color(0.572549, 0.729412, 1)
#		BeamColor.GOLD: return Color(0.572549, 0.729412, 1)
#		BeamColor.GREEN: return Color(0.572549, 0.729412, 1)
#		BeamColor.LIME: return Color(0.572549, 0.729412, 1)
#		BeamColor.MAGENTA: return Color(0.572549, 0.729412, 1)
#		BeamColor.ORANGE: return Color(0.572549, 0.729412, 1)
#		BeamColor.PURPLE: return Color(0.572549, 0.729412, 1)
#		BeamColor.RED: return Color(0.572549, 0.729412, 1)
#		BeamColor.VIRIDIAN: return Color(0.572549, 0.729412, 1)