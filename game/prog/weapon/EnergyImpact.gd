extends Damaging


export(float) var max_age := 1.0


var done := false
var done_in := 0.0 setget end_in


func _ready() -> void:
	assert(belongs_to)
#	$Particles.emitting = false
	connect("body_entered", self, "_on_hit")
	return


func _on_hit(victim: PhysicsBody) -> void:
	var already_hit := self.excluded_bodies.has(victim)
	if is_instance_valid(victim) \
			and victim.has_method("take_damage") \
			and victim != self.belongs_to \
			and not already_hit:
#		print(victim.display_name, "'s ",victim.name, " was hit by ", self.belongs_to.display_name, "'s EnergyImpact")
		if !victim.take_damage(self.belongs_to.display_name, 0):
			print("... but it was not effective")
		assert(not excluded_bodies.has(victim))
		excluded_bodies.push_front(victim)
		assert(excluded_bodies.has(victim))
	return


var age := 0.0
var done_for := 0.0
func _physics_process(delta: float) -> void:
	age += delta
	if done || age > max_age:
		$CollisionShape.disabled = true
		$Particles.emitting = false
		if done_for >= $Particles.lifetime:
			self.queue_free()
		done_for += delta
	elif done_in > 0.0:
		done_in -= delta
		if done_in <= 0.0:
			done = true

#	for body in self.get_overlapping_bodies():
#		if body is PhysicsBody \
#				&& body.has_method("take_damage") \
#				&& body.prog != self.belongs_to:
#			_on_hit(body)
	return


func sparkle(pos: Vector3) -> void:
	self.global_transform.origin = pos
	$Particles.emitting = true
	return


func end():
	done = true
	return

func end_in(time) -> void:
	done_in = time
	return
