extends Area
class_name Bot

export(float) var accuracy: float = 0.5
#onready var desired_accuracy: float = self.accuracy
export(float) var aggressivity: float = 0.5
export(float) var passivity: float = 0.5

export(NodePath) var prog_path: NodePath
export(NodePath) var jumping_path: NodePath
export(NodePath) var attacking_path: NodePath
export(NodePath) var orienting_path: NodePath


var target: Prog = null


var _look_pos_goal := Vector3(0,0,0)
var _lose_target_timer := 0.0


onready var prog := get_node(prog_path) as Prog
onready var jumping := get_node(jumping_path) as Jumping
onready var attacking := get_node(attacking_path) as Attacking
onready var orienting := get_node(orienting_path) as Orienting
onready var awareness_area: Area2D = get_node("AwarenessArea")


func _ready() -> void:
	$DecisionDelay.connect("exec_attack", self, "_exec_attack")
	$DecisionDelay.connect("exec_jump", self, "_exec_jump")


func _exec_attack(loc: Vector2) -> void:
	if !self.attacking.is_attacking:
		self.attacking.attack(Util.to_3d_ground(loc))
	return


func _exec_jump(loc: Vector2) -> void:
	self.jumping.queue_jump(loc)
	return


func _physics_process(delta: float) -> void:
	if is_network_master() and not self.prog.is_incapacitated():
		var pos := Util.to_2d(self.prog.puppet_pos)
		# Fake mouse motion
		var move_fake_mouse = rand_range(0, 100) <= delta * 50
		if move_fake_mouse:
			self._look_pos_goal = Util.to_3d_ground(Util.rand_loc(pos, 0, 800))
		var dir_change_speed: float = delta * rand_range(0.3, 2)
		var new_mouse_pos = self.orienting.oriented_towards\
				.linear_interpolate(self._look_pos_goal, dir_change_speed)
		self.orienting.oriented_towards = new_mouse_pos

		var do_attack = $DecisionDelay.is_stopped() \
			&& !self.attacking.is_attacking \
			&& ai_attack(delta)
		if do_attack:
			var from: Vector2 = Util.to_2d(self.prog.goal_pos)
			var aim_pos: Vector2 = take_aim(self.target, self.accuracy)
			var vec_diff: Vector2 = aim_pos - from
			var vec_maxed: Vector2 = \
				self.attacking.max_range * vec_diff.normalized()
			var max_length: float = (vec_maxed * Vector2(1, 2)).length()
			var in_range: bool = vec_diff.length() < max_length
			if in_range:
				$DecisionDelay.attack_decision(aim_pos)
				self.prog.rset("oriented_towards", Util.to_3d_ground(aim_pos))

		if self.passivity < 1.0 && !do_attack:
			var jump_to = ai_jump(delta)
			if jump_to != Vector2():
				$DecisionDelay.jump_decision(jump_to)
				self.prog.rset("oriented_towards", jump_to)

	return


func acquire_target(possible_targets: Array) -> Prog:
	var valid_targets: Array = []
	for tgt in possible_targets:
		if tgt is Prog and tgt != self.prog: # lol
			valid_targets.append(tgt)
	var valid_c: int = valid_targets.size()
	return valid_targets[randi() % valid_c] \
		if valid_c > 0 else null


func take_aim(tgt: Prog, acc: float) -> Vector2:
	# Aim at target or where it's going
	var aim_pos: Vector2 = Util.to_2d(tgt.goal_pos)
	var tgt_vec: Vector2 = aim_pos - Util.to_2d(self.prog.goal_pos)
	# Overshoot between 0 and 2 meters like a human might
	var overshoot := tgt_vec.normalized() * Util.rand_gaussian(1.0, 1.0)
	# How far a miss
	var hit_off: float = abs(Util.rand_gaussian(0.0, (1.0 - acc) * 5))
	var angle: float = rand_range(0, PI/2)
	var miss_offset: Vector2 = Vector2(hit_off, 0.0).rotated(angle)
	return aim_pos + overshoot + miss_offset


func ai_attack(delta: float) -> bool:
	# Maybe attack
	if self.target and not self.target.is_queued_for_deletion():
		if self.attacking.is_attacking:
			return false
		elif not self.target.is_vulnerable():
			self.target = null
			return false
		else:
			# Lose the target after 5 seconds of no visual
			if not overlaps_body(self.target):
				if self._lose_target_timer > 0.0:
					self._lose_target_timer -= delta
					if self._lose_target_timer <= 0.0:
						self.target = null
						return false
				else:
					self._lose_target_timer = 5.0

			var jump_desync: float = min(1.0, \
				abs(self.target.puppet_pos.distance_to(self.target.goal_pos) \
					- self.prog.puppet_pos.distance_to(self.prog.goal_pos)) \
					/ 5.0)
			var moving_fac: float = 1.0 - jump_desync
			var avg_atks_per_sec_max: float = 5.0
			var emotion: float = \
				(0.5 * (1 - self.passivity) \
					+ 2.0 * self.aggressivity) \
				/ 2.0
			var chance: float = \
				emotion \
				* moving_fac \
				* 100.0 * avg_atks_per_sec_max
			return rand_range(0.0, 100.0) <= chance * delta
	else:
		self.target = acquire_target(get_overlapping_bodies())
		return false


func ai_jump(delta: float) -> Vector2:
	# Maybe jump
	# Much more likely to jump if engaged in combat and target isn't moving
	# or is close to landing.
	var danger := self.target \
		&& self.target.puppet_pos.distance_to(self.target.goal_pos) <= 1.5
	var passivity_ := self.passivity * 0.1 if danger else self.passivity
	# Less likely to queue jump than to make new jump
	var is_jumping_fac := 150 if self.jumping.is_jumping else 200
	var chance_of_jumping: float = (1 - passivity_) * is_jumping_fac
	if rand_range(0, 100) <= chance_of_jumping * delta:
		var from: Vector2 = Util.to_2d(self.prog.goal_pos)
		# Make sure bot doesn't go too far from the nearest self.prog
		# and don't bother trying to queue an invalid jump
		var stay_close_to := \
			Util.to_2d(self.target.global_transform.origin) if self.target \
			else Vector2(0,0)
		var loc: Vector2 = Util.rand_loc(from, 10, self.jumping.max_range)
		var max_iter := 5
		while max_iter > 0 && not can_see_from(stay_close_to, loc):
			var next_loc_1 := Util.rand_loc(from, 1, self.jumping.max_range)
			var next_loc_2 := Util.rand_loc(from, 1, self.jumping.max_range)
			var nl1_dist := stay_close_to.distance_to(next_loc_1)
			var nl2_dist := stay_close_to.distance_to(next_loc_2)
			loc = next_loc_1 if nl1_dist < nl2_dist \
				else next_loc_2
			max_iter -= 1
		return loc
	else:
		return Vector2()


func can_see_from(point: Vector2, from: Vector2) -> bool:
	var dist_to_tgt: float = from.distance_to(point)
	return dist_to_tgt <= self.awareness_area.shape.radius * 2