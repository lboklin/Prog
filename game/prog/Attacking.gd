extends Component
class_name Attacking

signal attacking_target
signal cancelled_attack(atk_pos)

export(NodePath) var beam_weapon_1
export(NodePath) var beam_weapon_2
export(NodePath) var jumping := NodePath("../Jumping")
export(NodePath) var rolling := NodePath("../Rolling")
export var stop_when_attacking := true


var is_attacking := false
var max_range: float
var attack_location := Vector3()# setget ,get_attack_location
#func get_attack_location() -> Vector3:
##	return get_by_last_activated()[0].attacking_pos
#	return get_general_attack_position()


var _weapons_active := 0
var _queued_attacks := PoolVector3Array()
var _last_activated_wep: int = 0


onready var _rolling := get_node(rolling)
onready var _jumping := get_node(jumping)
onready var _beam_weapon_1 := get_node(self.beam_weapon_1) as BeamWeapon
onready var _beam_weapon_2 := get_node(self.beam_weapon_2) as BeamWeapon


func _ready() -> void:
	self.max_range = max(\
			self._beam_weapon_1.max_attack_range, \
			self._beam_weapon_2.max_attack_range)

	connect_weapon(self._beam_weapon_1)
	connect_weapon(self._beam_weapon_2)
	self._jumping.connect("just_landed", self, "_on_landed")
	self._jumping.connect("just_jumped", self, "_on_jumped")
#	self._rolling.connect("started_rolling", self, "stop_attacking")
	return


func _done_attacking():
	return


func _on_landed(_at: Vector3, _has_queued: bool):
	return


func _on_jumped(_to: Vector3):
	if not (self.prog.can_jump_while_attacking or self.prog.can_attack_while_jumping):
		stop_attacking()
	return


func _on_weapon_cancelled(atk_pos) -> void:
#	self.attack(atk_pos)
#	self._queued_attacks.append(atk_pos)
	self._queued_attacks = PoolVector3Array([atk_pos])
	return


func _on_weapon_deactivated() -> void:
	self._weapons_active -= 1
	if _weapons_active < 1: # Was only active weapon
		self.is_attacking = false
	return


func _on_weapon_activated() -> void:
	self._weapons_active += 1
	self.is_attacking = true
	emit_signal("attacking_target")
#	print("acquired_target")
	return


func _process(_delta: float) -> void:
	if is_network_master():
		if self.prog.is_incapacitated():
			self._queued_attacks = PoolVector3Array()
			return
		elif 0 < self._queued_attacks.size() and attack(self._queued_attacks[0]):
			self.attack_location = get_general_attack_position()
			self._queued_attacks.remove(0)
	return


func connect_weapon(wpn: BeamWeapon) -> void:
	wpn.connect("activated", self, "_on_weapon_activated")
	wpn.connect("deactivated", self, "_on_weapon_deactivated")
	wpn.connect("cancelled", self, "_on_weapon_cancelled")
	return


# Unless prog can attack during a jump, they have to be standing still and
# not already be attacking.
func can_attack(_atk_pos := Vector3()) -> bool:
	if self.prog.can_attack_while_jumping or not self._jumping.is_jumping:
		var wpn := get_best_weapon()
		return wpn and wpn.side.can_point_at(_atk_pos)
	else:
		return false


func weapon_is_available(wpn: BeamWeapon, override_command := false) -> bool:
	return not wpn.is_firing \
		and not wpn.side.is_blocked() \
		and (not has_target(wpn) or override_command) \
		and (not self._jumping.is_jumping or self.prog.can_attack_while_jumping) \
		and wpn.cooldown_timer <= 0

#
func activate_weapon(wpn: BeamWeapon, pos: Vector3) -> bool:
	if wpn and wpn.side.can_point_at(pos) and wpn.activate(pos):
		self._last_activated_wep = 1 if wpn == self._beam_weapon_1 else 2
		return true
	else:
		return false


func stop_attacking() -> void:
	var wpn1 := self._beam_weapon_1
	var wpn2 := self._beam_weapon_2

	if wpn1.is_firing:
		wpn1.deactivate()
	elif wpn1.is_aiming:
		# This will trigger _on_weapon_cancelled,
		# but we'll clear the queue right after.
		self._beam_weapon_1.cancel_attack()
		emit_signal("cancelled_attack", Vector3())

	if wpn2.is_firing:
		wpn2.deactivate()
	elif wpn2.is_aiming:
		self._beam_weapon_2.cancel_attack()
#		emit_signal("cancelled_attack", wpn2.attacking_pos)
		emit_signal("cancelled_attack", Vector3())

	self._queued_attacks = PoolVector3Array()
	return


# Returns true if a weapon was available and has accepted the command.
# Hasn't necessarily fired yet.
func attack(pos: Vector3) -> bool:
	var prev_active_wpns := self._weapons_active

	# Successful attack
	if (not self._jumping.is_jumping or self.prog.can_attack_while_jumping) \
			and activate_weapon(get_best_weapon(pos), pos):
		if self.stop_when_attacking:
			self._rolling.stop()

		if not self.prog.is_vulnerable():
			Util.expire_timer(self.prog.invulnerability_timer)
	else:
		var q_c := self._queued_attacks.size()
		# queue is either empty or only has one which isn't same as pos
		if q_c < 1 or (q_c < 2 and self._queued_attacks[0] != pos):
			self._queued_attacks.append(pos)

	return prev_active_wpns < self._weapons_active


# Returns the attack location of an active weapon or if both are activated,
# the middle point between them.
func get_general_attack_position() -> Vector3:
	var wpns := get_by_last_activated()
	var wpn1_pos: Vector3 = wpns[0].attacking_pos
	var wpn2_pos: Vector3 = wpns[1].attacking_pos
	if wpn1_pos != Vector3() and wpn2_pos != Vector3():
		var prog_pos := self.prog.global_transform.origin
		var same_dir := 0 < (wpn1_pos - prog_pos).dot(wpn2_pos - prog_pos)
		return wpn1_pos.linear_interpolate(wpn2_pos, 0.5) if same_dir \
				else wpn1_pos
	elif wpn1_pos != Vector3():
		return wpn1_pos
	elif wpn2_pos != Vector3():
		return wpn2_pos
	else:
		return self._queued_attacks[0] if 0 < self._queued_attacks.size() \
				else Vector3()


## Return a weapon available for use or null if none are.
#func get_available_weapon(_atk_pos := Vector3()) -> BeamWeapon:
#	# Check the most recently activated weapon last
#	var wpns := get_by_last_activated(true) \
#			if _atk_pos == Vector3() or has_target() \
#			else get_by_nearest_side(_atk_pos)
#	var wpn1: BeamWeapon = wpns[0]
#	var wpn2: BeamWeapon = wpns[1]
#	if weapon_is_available(wpn1, true) and angle_ok(wpn1, _atk_pos):
#		return wpn1
#	elif weapon_is_available(wpn2, true) and angle_ok(wpn1, _atk_pos):
#		return wpn2
#	else:
#		return null


func get_best_weapon(_atk_pos := Vector3()) -> BeamWeapon:
	var no_angle_check := _atk_pos == Vector3()
	var wpns := get_by_last_activated(true)
	var wpn1: BeamWeapon = wpns[0]
	var wpn2: BeamWeapon = wpns[1]
	var wpn1_available := weapon_is_available(wpn1)
	var wpn2_available := weapon_is_available(wpn2)
	if wpn1_available and wpn2_available:
		return wpn1 if (no_angle_check or angle_ok(wpn1, _atk_pos)) else wpn2
	elif wpn1_available and (no_angle_check or angle_ok(wpn1, _atk_pos)):
		return wpn1
	elif wpn2_available and (no_angle_check or angle_ok(wpn1, _atk_pos)):
		return wpn2
	else:
		return null


func angle_ok(wpn: BeamWeapon, pos: Vector3) -> bool:

#	var other_pos: Vector3 = wpn2.attacking_pos if wpn2.is_active \
#		else pos
#	var vec := pos - self.prog.global_transform.origin
#	var angle := vec.angle_to(self.prog.get_alignment_closest_to(pos))
#	match [wpn1.attacking_pos, wpn2.attacking_pos]:
#		[Vector3(), Vector3()]:
#	return angle < wpn.side.max_angle
	return wpn.side.can_point_at(pos)


#func angle_ok(wpn1: BeamWeapon, pos: Vector3, wpn2: BeamWeapon) -> bool:
#
#	var other_pos: Vector3 = wpn2.attacking_pos if wpn2.is_active \
#		else pos
#	var angle := angle_between(pos, other_pos)
##	match [wpn1.attacking_pos, wpn2.attacking_pos]:
##		[Vector3(), Vector3()]:
#	return angle < wpn1.side.max_angle


# Angle between the global positions p1 and p2 relative to the prog
func angle_between(p1: Vector3, p2: Vector3) -> float:
	var pos := self.prog.global_transform.origin
	var p1_rel := p1 - pos
	var p2_rel := p2 - pos
	return abs(p1_rel.angle_to(p2_rel))


func get_by_nearest_side(pos: Vector3) -> Array:
	var nearest := get_closest_weapon(pos)
	return [self._beam_weapon_1, self._beam_weapon_2] \
			if self._beam_weapon_1 == nearest \
			else [self._beam_weapon_2, self._beam_weapon_1]


func get_closest_weapon(pos: Vector3) -> BeamWeapon:
	var prog_pos := self.prog.global_transform.origin
	var side1_vec := self._beam_weapon_1.side.global_transform.origin - prog_pos
	var atk_vec := pos - self.prog.global_transform.origin
	return self._beam_weapon_1 if 0 <= side1_vec.dot(atk_vec) \
			else self._beam_weapon_2


# Return an array of the weapons, from most recently activated to least
func get_by_last_activated(reversed: bool = false) -> Array:
	var arr := [self._beam_weapon_1, self._beam_weapon_2] if self._last_activated_wep != 2 \
		else [self._beam_weapon_2, self._beam_weapon_1]
	if reversed:
		arr.invert()
	return arr


# Return the last activated weapon if still active, otherwise null
func get_active_weapon() -> BeamWeapon:
	var wpns := get_by_last_activated()
	for wpn in wpns:
		if wpn.is_active:
			return wpn
	return null


func has_target(wpn = null) -> bool:
	if wpn != null: # If weapon specified, return only whether it has a target
		return wpn.is_active
	else: # If unspecified, return whether any weapon has an attack pos
		return self._beam_weapon_1.is_active or self._beam_weapon_2.is_active \
				or 0 < self._queued_attacks.size()
#		for wpn in get_by_last_activated():
#			if wpn.is_active:
#				return true
#		return false