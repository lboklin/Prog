extends Spatial
class_name Side

enum Side { LEFT, RIGHT }

export(NodePath) var prog_path := NodePath("../")
onready var prog := get_node(prog_path) as Prog

onready var mesh_instance := $Side

onready var _offset := self.translation.x

# Maximum range at which you can rotate downwards to look at ground
export(float, 0, 50) var max_look_range: float = 8
# Maximum distance the sides can be extruded
export(float, 0, 0.3) var max_open: float = 0.2 setget set_max_open
func set_max_open(val):
	self.max_angle = min(45.0 * self.max_open / 0.2, 90.0)
	max_open = val
	return

# How quickly the sides rotate
export(float, 0, 1) var side_align_time: float = 0.2
# How quickly the sides open
export(float, 0, 1) var side_open_time: float = 0.2
# How quickly the sides close
#export(float, 0, 100) var side_close_speed: float = 10
export(float, 0, 10) var inaccuracy_tolerance: float = 2


var face_pos := Vector3() setget set_face_pos
func set_face_pos(val: Vector3) -> void:
	if val == Vector3():
		face_pos = val
		self.is_aligned = self.reset_rot(0.0)
	else:
		# Remember our rotation before aligning (to get the time-to-align right)
		self._begin_align_rot = self.global_transform.basis.get_rotation_quat()
		# Max range adjustment
		var pos := self.global_transform.origin
		face_pos = pos + Util.clamp_length(val - pos, 0.0, self.max_look_range)
		# We could do it the correct way or the cheap way.
		# For now I'm going with the former as doing it the cheap way costs
		# us a frame of delayed activation when already aligned and the only
		# benefit is a saving a few calculations.
		self.is_aligned = self.align_rot(face_pos, 0.0)
#		self.is_aligned = true
	return

var open_amount := 0.0
var is_open := false
var is_aligned := true


onready var _orig_rot := self.global_transform.basis.get_rotation_quat()
onready var _begin_align_rot := self.global_transform.basis.get_rotation_quat()
# Maximum angle of the sides' rotation around the up axis
onready var max_angle: float = min(45.0 * self.max_open / 0.2, 90.0)


func _ready() -> void:
	self.mesh_instance.mesh = self.mesh_instance.mesh.duplicate()
	var mat: ShaderMaterial = preload("res://prog/res/prog-side.material").duplicate()
	self.prog.connect("colors_changed", self, "update_material_color", [mat])
	return


func _process(delta: float) -> void:
	self.is_aligned = self.align_rot(self.face_pos, delta)
	self.is_open = self.open_side(delta, open_amount)
	return


# The side can get hit and take damage
func take_damage(killer_name: String, damage: int = 0, ignore_invulnerability := false) -> bool:
	return self.prog.take_damage(killer_name, damage, ignore_invulnerability)


func update_material_color(mat: ShaderMaterial) -> void:
	mat.set("shader_param/color", self.prog.secondary_color)
	self.mesh_instance.mesh.surface_set_material(0, mat)
	return


# If the side is unable to extend itself
func is_blocked() -> bool:
	var tf := self.global_transform
	var prog_tf := self.prog.global_transform

	var side_vec := tf.origin - prog_tf.origin
	var up_ang := side_vec.angle_to(Vector3.UP)
	return deg2rad(100) < up_ang


func can_point_at(pos: Vector3) -> bool:
#	if is_blocked():
#		return false
#	else:
	var vec := pos - self.global_transform.origin
	var fw := self.prog.get_alignment_closest_to(pos)
	var ang: float = fw.angle_to(vec)
	return ang < deg2rad(self.max_angle)


func open_side(step: float, amount: float = self.open_amount) -> bool:
	var amount_ := (min(amount, max_open) \
			if (self.rotation * Vector3(1,1,0)).length_squared() > 0 else 0.0)
	var final_dpl := self._offset + sign(self._offset) * amount_
	var total_time := self.side_open_time * (2 if amount < self.open_amount else 1)
	var x: float = lerp(self.translation.x, final_dpl, min(1, step / total_time))
	if self.translation.x != x:
		self.translation.x = x
	return abs(final_dpl - x) < 0.001


func reset_rot(step) -> bool:
	var cur_rot := self.transform.basis.get_rotation_quat()
	var a := min(1, self.side_align_time * step * 30)
	var rot: Quat = cur_rot.slerp(self._orig_rot, a)
	self.rotation = rot.get_euler()
	return rot == self._orig_rot


var _align_timer := 0.0
func align_rot(loc: Vector3, step: float, _lock_axis: Vector3 = Vector3()) -> bool:
	if loc == Vector3(): # Derotate
		_align_timer = 0.0
		return reset_rot(step)
	else:
		_align_timer += step
		loc = Util.to_ground(loc)

		var g_tf := self.global_transform
		var prog_bs := self.prog.global_transform.basis

		var fw := self.prog.get_alignment_closest_to(loc)
		var up := prog_bs.x.cross(fw).normalized() * sign(self._offset)
		assert(fw.angle_to(loc) <= self.max_angle)
		var complete_rot: Quat = g_tf.looking_at(loc, up).basis.get_rotation_quat()

		var s := min(1, _align_timer / self.side_align_time)
		var next_bs := Basis(self._begin_align_rot.slerp(complete_rot, s))

		self.global_transform.basis = next_bs

		var cur_aim_pos := Plane(Vector3.UP, Util.GROUND_HEIGHT)\
				.intersects_ray(g_tf.origin, -next_bs.z)
		if cur_aim_pos:
			var aim_off := cur_aim_pos.distance_to(loc)
			# ---- DEBUG ----
#				if cur_aim_pos.distance_to(loc) < 5:
#					var player := get_tree().get_root().get_node("/root/GameRound/Players/Player")
#					player.draw_line_world(cur_aim_pos, loc, Color.red, 0.1)
#					player.spawn_click_indicator(cur_aim_pos, Vector3(), Color.red)
			# ---------------
			return aim_off < self.inaccuracy_tolerance
		else:
			return false