extends RigidBody
class_name Prog


# Is emitted when a player has been killed with
# the id of the killed and the id of the killer as arguments.
signal player_killed(killer_name)

# Is emitted when a player has respawned with the id as argument.
signal player_alive

signal colors_changed
signal incapacitated
signal vulnerable
signal invulnerable

# Need this for smooth physics manipulation by external nodes
signal integrate_forces(state)


# Special abilities / mechanics
export(bool) var can_attack_while_jumping: bool = false

export(bool) var can_jump_while_attacking: bool = false \
	setget ,get_can_jump_while_attacking
func get_can_jump_while_attacking() -> bool:
	return self.can_attack_while_jumping or can_jump_while_attacking

export(NodePath) var mesh_instance_path: NodePath = "MeshInstance"


# Your Prog's very own beautiful color scheme
var primary_color: Color
var secondary_color: Color
# Whether the primary color is the more vibrant one or the other way around
var primary_color_dominant: bool = true

# The name other players will be seeing
var display_name: String = "Dead Inside"

# Where we intend to be, whether that be where we are (no motion)
# or where we're jumping to.
remotesync var goal_pos := Vector3() setget set_goal_pos
func set_goal_pos(val: Vector3) -> void:
	goal_pos = Util.to_ground(\
			val if val != Vector3() else self.global_transform.origin)
	return

# The position the puppet is told it should be in
remotesync var puppet_pos := Vector3()
# Our health
remotesync var health := 100


# The Prog's mesh instance
onready var mesh_instance := get_node(mesh_instance_path) as MeshInstance
onready var raycast : = $RayCast as RayCast
onready var shape := $SphereShape as CollisionShape

# How long we're going to be invulnerable for
onready var invulnerability_timer := Util.new_timer(self, "Invulnerability")
# Respawn timer
onready var dead_timer := Util.new_timer(self, "Dead")
# Stunned timer
onready var stunned_timer := Util.new_timer(self, "Stunned")


func _ready():

	var mat: ShaderMaterial = preload("res://prog/res/prog-center.material").duplicate()
	self.mesh_instance.mesh = self.mesh_instance.mesh.duplicate()
	self.connect("colors_changed", self, "update_material_color", [mat])

	if is_network_master():
		self.dead_timer.connect("timeout", self, "_done_being_dead")
		self.invulnerability_timer.connect("timeout", self, "_done_being_invulnerable")
		self.stunned_timer.connect("timeout", self, "_done_being_stunned")

		var primary_hue: float = rand_range(0.0, 1.0)
		var secondary_hue: float = rand_range(0.0, 1.0)
		rpc("set_colors", primary_hue, secondary_hue, rand_range(0.0, 1.0) > 0.5)
	else: # If we're a puppet, we obey only the master - not physics!
		#self.mode = RigidBody.MODE_KINEMATIC # done in respawn
		pass

	respawn(Vector3())
	return


func _done_being_invulnerable():
	set_invulnerable(0)
	return


func _done_being_dead():
	self.respawn(Vector3())
	return


func _done_being_stunned():
	return


func _integrate_forces(state: PhysicsDirectBodyState) -> void:
	emit_signal("integrate_forces", state)
	state.integrate_forces()
	return


func _physics_process(_delta):
	var current_pos: Vector3 = self.global_transform.origin
	if not is_network_master(): # we're a puppet
		self.global_transform.origin = self.puppet_pos
		var dist := current_pos.distance_to(self.puppet_pos)
		var t := ease(_delta / max(1.0, dist), 0.3)
		self.global_transform.origin = \
				current_pos.linear_interpolate(self.puppet_pos, t)
		return
	elif is_incapacitated(): # We're unable to do anything
		return
	else: # We're network master and fully operable
		if self.is_falling_to_death():
			self.rpc("die", self.display_name, 2.0)
		rset("puppet_pos", current_pos)
	return


func is_on_floor() -> bool:
	raycast.cast_to = -self.global_transform.basis.inverse().y * 0.4
	return raycast.is_colliding()


func is_falling_to_death() -> bool:
	return self.linear_velocity.y < -20


func get_score() -> int:
	var game_round = get_node("/root/GameRound")
	return game_round.scorekeeper[self.display_name]


func is_incapacitated() -> bool:
	return any_active([self.dead_timer, self.stunned_timer])


func is_dead() -> bool:
	return !self.dead_timer.is_stopped()


func is_vulnerable() -> bool:
	return not any_active([self.dead_timer, self.invulnerability_timer])


func set_invulnerable(time: float) -> void:
	if time > 0:
#		self.hide() # temporary
		self.invulnerability_timer.start(time)
		emit_signal("invulnerable")
	else:
#		self.show() # temporary
		emit_signal("vulnerable")
	return


func get_side_closest_to(pos: Vector3) -> Vector3:
	var prog_tf := self.global_transform
	var vec := pos - prog_tf.origin
	return prog_tf.basis.x * Util.int_bool(0 < (prog_tf.basis.x.dot(vec)))


func get_alignment_closest_to(pos: Vector3) -> Vector3:
	var prog_tf := self.global_transform
	var prog_bs := self.radial_symmetry(prog_tf.basis)
	var vec := pos - prog_tf.origin
	return prog_bs.z * Util.int_bool(0 < (prog_bs.z.dot(vec)))


# TODO: Implement health
func take_damage(\
		inflicter: String, \
		damage: int, \
		ignore_invulnerability := false) \
		-> bool:
	if is_vulnerable() or ignore_invulnerability:
		self.health -= damage
		if self.health <= 0:
			rpc("die", inflicter, 2.0)
		else:
			rpc("stun", inflicter, 0.1)
		return true
	else:
		return false


remotesync func respawn(pos: Vector3 = Util.to_3d_ground(Util.rand_loc(Vector2(0,0), 0.0, 10.0))):
	self.global_transform = Transform(Basis(), pos + Vector3(0,0.5,0))
	self.linear_velocity = Vector3()
	self.angular_velocity = Vector3()
	self.mode = RigidBody.MODE_RIGID if is_network_master() else RigidBody.MODE_KINEMATIC
	self.shape.disabled = false
	self.show()
	set_invulnerable(30)
	# TODO: Visual indication that we're invulnerable

	self.dead_timer.stop() # just in case it's a dev cheat respawn
	self.stunned_timer.stop()
	self.health = 100
	emit_signal("player_alive")
	return


# TODO: Implement death visuals
remotesync func die(killer_name: String, respawn_time: float) -> void:
	self.dead_timer.start(respawn_time)
	self.mode = RigidBody.MODE_STATIC
	self.shape.disabled = true
	self.hide()
	set_invulnerable(respawn_time)
	emit_signal("player_killed", killer_name)
	emit_signal("incapacitated")
	return


remotesync func stun(_stunned_by: String, stun_time: float) -> void:
	self.stunned_timer.start(stun_time)
	emit_signal("incapacitated")
	return


master func sync_colors() -> void:
	rpc("set_colors", \
		primary_color.h, \
		secondary_color.h, \
		primary_color_dominant)
	return


remotesync func set_colors( \
		primary_hue: float, \
		secondary_hue: float, \
		dominant_prim: bool) \
		-> void:
	var primary: Color
	var secondary: Color
	if dominant_prim:
		primary = Color.from_hsv(primary_hue, 0.4, 0.5)
		secondary = Color.from_hsv(secondary_hue, 0.24, 0.6)
	else:
		primary = Color.from_hsv(primary_hue, 0.25, 0.6)
		secondary = Color.from_hsv(secondary_hue, 0.3, 0.5)

	primary_color = primary
	secondary_color = secondary
	primary_color_dominant = dominant_prim
	emit_signal("colors_changed")
	return


func update_material_color(mat: ShaderMaterial) -> void:
	mat.set("shader_param/color", self.primary_color)
	self.mesh_instance.mesh.surface_set_material(0, mat)
	return


static func any_active(timers: Array) -> bool:
	for timer in timers:
		if !timer.is_stopped():
			return true
	return false


static func radial_symmetry(bs: Basis) -> Basis:
	var up := Vector3(0,1,0)
	var rad_sym_back := bs.x.cross(up).normalized()
	var rad_sym_up := rad_sym_back.cross(bs.x).normalized()
	return Basis(bs.x, rad_sym_up, rad_sym_back)


# ----- Debug
#onready var dir_indicator := show_dir_indicator(Vector3(0,1,0))
#
#func show_dir_indicator(dir: Vector3, color: Color = Color.white) -> MeshInstance:
#	 var msh: Mesh = preload("res://tests/dir_indicator.tres").duplicate(true)
#	 var mshi := MeshInstance.new()
#	 mshi.mesh = msh
#	 self.add_child(mshi)
#	 set_dir_indicator_dir(dir, color, mshi)
#	 return mshi
#
#func set_dir_indicator_dir(dir: Vector3, color: Color = Color.white, mshi: MeshInstance = self.dir_indicator) -> void:
#		if dir == Vector3():
#			mshi.hide()
#		else:
#			mshi.show()
#			var tf := self.global_transform
#			var pos := tf.origin + (dir.normalized() * 0.5)
#			mshi.look_at_from_position(pos, pos + dir, Vector3(1,0,0))
#			mshi.mesh.material.albedo_color = color
#		return
# -----