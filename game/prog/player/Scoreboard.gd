extends VBoxContainer

var player_name: String


func add_entry(entry_name: String) -> NameScore:
	var name_score = preload("res://prog/player/NameScore.tscn").instance()
	name_score.is_player = entry_name == player_name
	name_score.set_name_score(entry_name, 0)
	add_child(name_score)
	return name_score


func remove_entry(entry_name: String) -> void:
	for entry in get_children():
		var name_rtl: Label = entry.name_label
		if name_rtl && name_rtl.text == entry_name:
			entry.queue_free()


func get_entry(entry_name: String) -> NameScore:
	for child in get_children():
		var name_rtl: Label = child.name_label
		if name_rtl && name_rtl.text == entry_name:
			return child
	# If the entry wasn't found, add it
	return add_entry(entry_name)


func update_entry(entry_name: String, entry_score: int) -> void:
	var prev_i: int = 0
	var i: int = 0
	var prev: NameScore
	for ns in get_children():
		if !prev:
			prev = ns
		# If previous entry has a lower score than this one
		elif prev.entry_score < ns.entry_score:
			# Swap their place
			move_child(ns, prev_i)
			move_child(prev, i)
		else:
			prev_i = i
			prev = ns


		var name_rtl: Label = ns.name_label
		if name_rtl && name_rtl.text == entry_name:
			# Update entry and be done
			ns.set_name_score(entry_name, entry_score)
			return

		i += 1

	# If the entry wasn't found, add it
	add_entry(entry_name)
	return