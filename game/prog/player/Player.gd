extends Spatial
class_name Player


export(NodePath) var prog_path: NodePath

export var third_person_view := false setget set_tpv
func set_tpv(is_tpv: bool):
	if is_tpv != third_person_view and self.has_node("PlayerCam"):
		self.remove_child(self.get_node("PlayerCam"))

	if is_tpv:
		var player_cam: Spatial = self.camera_tpv_tscn.instance() as Spatial
		player_cam.prog_path = self.prog_path
		self.add_child(player_cam)
	else:
		var player_cam: Spatial = self.camera_tscn.instance() as Spatial
		player_cam.prog_path = self.prog_path
		player_cam.canvas_layer = self.canvas_layer
		self.add_child(player_cam)

	third_person_view = is_tpv
	return


var camera_tpv_tscn: PackedScene = preload("res://prog/player/PlayerCamTPV.tscn")
var camera_tscn: PackedScene = preload("res://prog/player/PlayerCam.tscn")


onready var prog: Prog = get_node(prog_path)
onready var canvas_layer: CanvasLayer = $CanvasLayer


func _ready() -> void:
	self.third_person_view = false
	return


func _input(ev: InputEvent) -> void:
	if ev.is_action_pressed("switch_camera"):
		self.third_person_view = not self.third_person_view

	elif ev.is_action_pressed("self_destruct"):
		self.prog.emit_signal("player_killed", self.prog.display_name) # Killed self

	elif ev.is_action_pressed("dev_respawn"):
		self.prog.emit_signal("player_alive")

	elif ev.is_action_pressed("spawn_enemy"):
		self.spawn_bot()

	elif ev.is_action_pressed("spawn_test_box"):
		var box := preload("res://tests/RigidBox.tscn").instance() as RigidBody
		var box_parent = get_node("/root/GameRound/Players")
		box_parent.add_child(box)
		box.global_transform.origin = self.prog.global_transform.origin + Vector3(0,3,0)

	elif ev.is_action_pressed("increase_game_speed"):
		Engine.time_scale += 0.25
	elif ev.is_action_pressed("decrease_game_speed"):
		Engine.time_scale -= 0.25

	elif ev.is_action_pressed("change_colors"):
		var dom_prim: bool = rand_range(0.0, 1.0) > 0.5
		prog.rpc(\
			"set_colors", \
			rand_range(0.0, 1.0), \
			rand_range(0.0, 1.0), dom_prim)
	return


func spawn_bot() -> void:
	var game_round = get_node("/root/GameRound")
	var id: int = get_tree().get_network_unique_id()
	# We spawn bot separately because we need to generate a name
	game_round.rpc("new_bot", id)
	return