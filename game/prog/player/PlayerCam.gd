extends Spatial


# Exported node dependencies

export(NodePath) var prog_path: NodePath
#export(NodePath) var canvas_layer_path := NodePath("CanvasLayer")


# Exported properties

export(bool) var show_click_indicator := true
#export(bool) var dev_mode := true
export(bool) var stop_in_place := false
export(float, 0.0, 1.0) var mouse_button_hold_delay := 0.5


# Properties

var pixels_per_meter: Vector2
var canvas_layer: CanvasLayer


# Private member variables

var _mb1_held_time := -1.0 # -1.0 if not held down
var _move_indicator: Indicator = null setget set_persistent_indicator#,get_persistent_indicator
var _is_wasding := false


# Node dependencies

onready var pointer: Pointer = preload("res://prog/player/Pointer.tscn").instance()
onready var camera: Camera = $Swivel/Camera
#onready var canvas_layer: CanvasLayer = get_node(self.canvas_layer_path)
onready var prog: Prog = get_node(self.prog_path)

onready var rolling := self.prog.get_node("Rolling") as Rolling
onready var jumping := self.prog.get_node("Jumping") as Jumping
onready var attacking := self.prog.get_node("Attacking") as Attacking
onready var orienting := self.prog.get_node("Orienting") as Orienting
onready var camera_max_size: float = 10.5


func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	self.pointer.update_continuously = false
	self.pointer.connect("pointer_updated", self, "_on_pointer_updated")
#	self.rolling.connect("started_rolling", self, "_on_start_rolling")
	self.prog.get_parent().add_child(self.pointer)

	self.pixels_per_meter = get_ppm()
	zoom(-0.01)

	self.connect("tree_exited", self, "_on_tree_exited")
	return


func _on_tree_exited():
	if is_instance_valid(self.pointer):
		self.pointer.queue_free()
	self.queue_free()
	return


func _on_pointer_updated(pos: Vector3):
	self.orienting.oriented_towards = pos
	if Input.is_mouse_button_pressed(1):
		self._mb1_held_time += self.get_physics_process_delta_time()
		self.rolling.roll_to(self.pointer.last_position)
		if self.mouse_button_hold_delay <= self._mb1_held_time:
			if not is_instance_valid(self._move_indicator):
				self._move_indicator = spawn_click_indicator(null)
			self._move_indicator.pause_after(0.0)
	return


func _on_indicator_done(_ind: Indicator):
	self._move_indicator = null
	return


func _process(_delta: float) -> void:
	var pos: Vector3 = Util.to_ground(self.prog.global_transform.origin)
	var cur_pos := self.global_transform.origin
	var dist_weight := clamp(ease((pos - cur_pos).length() / 3.0, 5.0), 0.0, 1.0)
	self.global_transform.origin = lerp(cur_pos, pos, dist_weight)

	if self.jumping.is_jumping:
		return

	var dir := Vector3()
	var wasding := false
	if Input.is_action_pressed("move_left"):
		dir += Vector3.LEFT
		wasding = true
	if Input.is_action_pressed("move_right"):
		dir += Vector3.RIGHT
		wasding = true
	if Input.is_action_pressed("move_up"):
		dir += Vector3.FORWARD
		wasding = true
	if Input.is_action_pressed("move_down"):
		dir += Vector3.BACK
		wasding = true

	if wasding:
		self._is_wasding = true

		if dir != Vector3():
			if not is_instance_valid(self._move_indicator):
				self._move_indicator = spawn_click_indicator(null)
				self._move_indicator.pause_after(0.3)
			dir = dir.normalized().rotated(Vector3.UP, PI/4)
			var max_dist := 12.0
			var dist := max_dist if Input.is_key_pressed(KEY_SHIFT) else 5.0
			var motion := dir * dist
			var prev_vec := self.prog.goal_pos - pos
			var dr := motion.length() / max(0.1, prev_vec.length())
			var a := ease(dr, -0.1)
			var vec := prev_vec.linear_interpolate(motion, _delta * a * 2)
			var maxed := Util.clamp_length(vec, 0.5, max_dist)
			var dest := pos + maxed
			self.rolling.roll_to(dest)

			if is_instance_valid(self._move_indicator):
				self._move_indicator.to_position(dest)
				self._move_indicator._age = 0.3
	elif self._is_wasding:
		if is_instance_valid(self._move_indicator):
			self._move_indicator.resume()
	return


func _input(ev: InputEvent) -> void:
	if ev is InputEventMouseMotion:
		self.pointer.queue_update()

	elif ev is InputEventKey:
		var evk := ev as InputEventKey

		if evk.scancode == KEY_SHIFT:
			if evk.is_pressed():
				if not self._is_wasding:
					self.rolling.stop()
					self.orienting.upright = false
					if Input.is_action_pressed("move_to"):
						self.jumping.queue_jump(Util.to_2d(self.pointer.last_position))
						if self.show_click_indicator:
							spawn_click_indicator(null, self.jumping.destination)
			else:
				self.orienting.upright = true

	# Move action
	elif ev.is_action_pressed("move_to"):
		self._is_wasding = false

		# Jump to
		if Input.is_key_pressed(KEY_SHIFT):
			self._mb1_held_time = -1
			self.rolling.stop()
			self.jumping.queue_jump(Util.to_2d(self.pointer.last_position))
			if self.show_click_indicator:
				self._move_indicator = spawn_click_indicator(null, self.jumping.destination)
		# Roll to
		else:
			self._mb1_held_time = 0.0
			if is_instance_valid(self._move_indicator):
				self._move_indicator.follow(self.pointer)
				self._move_indicator.reset()
			else:
				self._move_indicator = spawn_click_indicator(self.pointer)
			# Actual rolling is handled in _on_pointer_updated.
			# Final rolling dest handled on action released.
	# Final move action
	elif ev.is_action_released("move_to"):
		self._is_wasding = false

		# Simple click to move
		if self._mb1_held_time < self.mouse_button_hold_delay:# \
#				and not Input.is_key_pressed(KEY_SHIFT):
			self.rolling.roll_to(self.pointer.last_position)

			if not Input.is_key_pressed(KEY_SHIFT) and is_instance_valid(self._move_indicator):
#				self._move_indicator.follow(null)
				self._move_indicator.resume()
				self._move_indicator.to_position(self.pointer.last_position)
		# Stop rolling towards pointer
		else:
			if self.stop_in_place:
				self.rolling.stop()
			else:
				self.rolling.roll_to(self.pointer.last_position)

			if is_instance_valid(self._move_indicator):
				self._move_indicator.resume()
				if self.stop_in_place:
					self._move_indicator.follow(self.prog)
				else:
					self._move_indicator.to_position(self.pointer.last_position)

		self._mb1_held_time = -1

	# Attack is being pressed
	elif ev.is_action_pressed("attack"):
		var from: Vector3 = self.jumping.destination if self.jumping.is_jumping \
				else Util.to_ground(self.prog.global_transform.origin)
		var max_length: float = self.attacking.max_range
		var vec := self.pointer.last_position - from
		# Draw a line from where beam will end (because of max range) to clicked
		# position for some extra visual feedback.
#		if show_click_indicator && from.distance_to(_mouse_collide_pos) > max_length:
#			var clr = prog.secondary_color
#			clr.s = 0.45
#			clr.v = 0.9
#			spawn_click_indicator(_mouse_collide_pos, from, max_length, clr)
		# Display an indicator to offer some visual feedback on input
		# when the attack won't be immediately executed
		var atk_pos := from + Util.clamp_length(vec, 0.0, max_length)
		if self.show_click_indicator:# and jumping.is_jumping:
			spawn_click_indicator(\
					null, \
					atk_pos, \
					self.prog.secondary_color)
		self.attacking.attack(atk_pos)

	elif ev.is_action_pressed("jump"):
		var pos := self.prog.goal_pos
		if not is_instance_valid(self._move_indicator):
			self._move_indicator = spawn_click_indicator(null)
			self._move_indicator.pause_after(0.3)
		else:
			self._move_indicator.to_position(pos)
			self._move_indicator.resume()
		self.jumping.queue_jump(Util.to_2d(pos))

	elif ev.is_action_pressed("zoom_in"):
		zoom(0.4)
	elif ev.is_action_pressed("zoom_out"):
		zoom(-0.4)

	return


func set_persistent_indicator(ind: Indicator) -> void:
	if ind == null:
		_move_indicator = null
		return
	elif ind != _move_indicator:
		if is_instance_valid(_move_indicator):
			_move_indicator.resume()
			_move_indicator = null

		if is_instance_valid(ind) and ind is Indicator:
			ind.connect("done_indicating", self, "_on_indicator_done", [ind])
			_move_indicator = ind
			return


func draw_line_screen(from: Vector2, to: Vector2, \
		color: Color = Color(0.75, 0.75, 0.75), fade_time: float = 1.0) \
		-> void:
	var line := preload("res://prog/player/Line.tscn").instance()
	line.points[0] = from
	line.points[1] = to
	line.color = color
	line.fade_time = fade_time
	line.player_cam = self

	canvas_layer.add_child(line)
	return

func draw_line_world(from: Vector3, to: Vector3, \
		color: Color = Color(0.75, 0.75, 0.75), fade_time: float = 1.0) \
		-> void:
	draw_line_screen(\
		camera.unproject_position(from), \
		camera.unproject_position(to), \
		color, \
		fade_time)
	return

# Really only useful for debug
func draw_square_world(center: Vector3, size: Vector2 = Vector2(1,1), color: Color = Color(0.75, 0.75, 0.75), fade_time := 1.0) -> void:
	var ul := center - Vector3(0.5 * size.x, 0, 0.5 * size.y)
	var ur := ul + Vector3(size.x,0,0)
	var dr := ul + Vector3(size.x,0,size.y)
	var dl := ul + Vector3(0,0,size.y)
	draw_line_world(ul, ur, color, fade_time)
	draw_line_world(ur, dr, color, fade_time)
	draw_line_world(dr, dl, color, fade_time)
	draw_line_world(dl, ul, color, fade_time)


# Display an indicator for where you clicked
func spawn_click_indicator(\
		follow: Spatial, \
		maxed: Vector3 = Vector3(), \
		color: Color = Color(1,1,1)) \
		-> Indicator:
	var pos := Util.to_ground(\
			follow.global_transform.origin if follow \
			else self.pointer.last_position)
	var indicator: Indicator = \
			preload("res://prog/player/Indicator.tscn").instance() \
			as Indicator
	indicator.modulation = color
	self.pointer.get_parent().add_child(indicator)
	indicator.follow(follow)
	if maxed == Vector3() or maxed.distance_squared_to(pos) < 0.01:
		indicator.global_transform.origin = pos
	else:
		draw_line_world(pos, maxed, color)
		indicator.global_transform.origin = maxed
#	indicator.global_transform.origin.y = Util.GROUND_HEIGHT
	assert(indicator.global_transform.origin != Vector3())
	assert(indicator is Indicator)
	return indicator


func spawn_bot() -> void:
	var game_round = get_node("/root/GameRound")
	var id: int = get_tree().get_network_unique_id()
	# We spawn bot separately because we need to generate a name
	game_round.rpc("new_bot", id)
	return


# Calculate pixels per meter (on the ground)
func get_ppm() -> Vector2:
	var tl := prog.global_transform.origin
	# To the right of tl on screen
	var tl_right := tl + Vector3(1,0,0).rotated(Vector3(0,1,0), $Swivel.rotation.y)
	# Below tl on screen
	var tl_down :=  tl + Vector3(0,0,1).rotated(Vector3(0,1,0), $Swivel.rotation.y)
	var tl_px := camera.unproject_position(tl)
	var tl_right_px := camera.unproject_position(tl_right) - tl_px
	var tl_down_px := camera.unproject_position(tl_down) - tl_px

	var ppm := Vector2(tl_right_px.x, tl_down_px.y) * 2
#	var ppm := Vector2(90, 45)
	return ppm


func zoom(amount: float) -> void:
	camera.size -= amount
	if amount < 0.0 && camera.size > camera_max_size + amount:
		camera.size = camera_max_size
	return