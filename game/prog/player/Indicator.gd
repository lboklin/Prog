extends Spatial
class_name Indicator


signal done_indicating


const ETERNITY = 99999.99


export(Color) var modulation := Color.whitesmoke
export(float,0.0,10.0) var lifetime := 0.6

export(NodePath) var move_to := NodePath("MoveTo")


var _age := 0.0
var _follow_target: Spatial = null


onready var _move_to := get_node(self.move_to) as Sprite3D

onready var _pause_after := ETERNITY

onready var _pause_for := ETERNITY setget set_pause_for
func set_pause_for(val):
	_pause_for = val
#	if val < 99:
#		breakpoint
	return

onready var _fading_step := self._move_to.modulate.a / self.lifetime
onready var _scaling_step := Vector3.ONE * self._move_to.scale / self.lifetime


func _ready() -> void:
	var clr: Color = self._move_to.modulate
	var orig_v: float = clr.v
	clr = modulation
	clr.v = orig_v
	self._fading_step = self._move_to.modulate.a / self.lifetime
	self._scaling_step = Vector3.ONE * self._move_to.scale / self.lifetime
	self._move_to.modulate = clr
	self._move_to.scale = Vector3(0,0,0)



func _process(delta: float) -> void:
	self.follow(self._follow_target, delta)
	if self.is_paused():
		self._pause_for -= delta
	elif self.lifetime <= self._age:
		emit_signal("done_indicating")
		self.queue_free()
#		self.call_deferred("queue_free")
	else:
		self._age += delta
		self._pause_after -= delta
		self._move_to.modulate.a = lerp(1.0, 0.0, self._age / self.lifetime)
		var max_scale := Vector3.ONE * 0.5
		self._move_to.scale = Vector3().linear_interpolate(max_scale, self._age / self.lifetime)


func reset() -> void:
	self._age = 0.0
	self._pause_for = ETERNITY
	self._pause_after = ETERNITY
	return


func follow(target: Spatial, speed := 0.0) -> void:
	if self._follow_target != target:
		self._follow_target = target
	if is_instance_valid(target):
		var tgt_pos := Util.to_ground(target.global_transform.origin)
		var cur_pos := self.global_transform.origin
	#	var dist_sq := cur_pos.distance_squared_to(pos)
		var dist := cur_pos.distance_to(tgt_pos)
		var t := ease(speed * 30 / max(1.0, dist), 0.3)
		self.global_transform.origin = \
				cur_pos.linear_interpolate(tgt_pos, t)
	return


func to_position(pos: Vector3) -> void:
	self._follow_target = null
	self.global_transform.origin = pos
	return

# when duration is -1 it will remain paused until resume() is called.
func pause_after(t: float, duration: float = ETERNITY) -> void:
	self._pause_after = t
	self._pause_for = duration
	return


func is_paused() -> bool:
	return self._pause_after <= 0.0 and 0.0 < self._pause_for and not self.is_queued_for_deletion()


func resume() -> void:
	# Let it run it's course
	self._pause_after = ETERNITY
	self._pause_for = ETERNITY
	return
