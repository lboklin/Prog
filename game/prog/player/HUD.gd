extends CanvasLayer
class_name HUD

export(NodePath) var game_round_path: NodePath
onready var game_round: Node = get_node(game_round_path)

export(NodePath) var player_path: NodePath
onready var player: Node = get_node(player_path)

onready var timer_label := find_node("RoundTimer") as Label
onready var scoreboard := find_node("Scoreboard") as Control
onready var respawn_label := find_node("RespawnTimer") as Label


func _ready():
	game_round.connect("score_updated", self, "_score_updated")
	game_round.connect("status_updated", self, "_status_updated")
	scoreboard.player_name = game_round.lobby.my_data.player_name
	return


func _process(_delta: float) -> void:
	# Round timer
	timer_label.set_text("ROUND: " + str(floor(game_round.round_timer)))

	# Respawn timer
	var prog: Prog = player.prog
	if prog:
		var respawn_timer = prog.dead_timer.time_left
		if respawn_timer > 0:
			respawn_label.set_text(\
				"Respawning in: " + str(respawn_timer).pad_decimals(2))
			respawn_label.show()
		else:
			respawn_label.hide()


func _score_updated(player_name: String, score: int) -> void:
	scoreboard.update_entry(player_name, score)
	return


func _status_updated(player_name: String, status: int) -> void:
	match status:
		GameRound.Status.DEAD:
			scoreboard.get_entry(player_name).set_dead()
		GameRound.Status.ALIVE:
			var entry: NameScore = scoreboard.get_entry(player_name)
			entry.set_dead(false)
		GameRound.Status.GONE:
			scoreboard.remove_entry(player_name)
	return