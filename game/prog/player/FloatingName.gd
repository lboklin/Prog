extends Sprite3D

export(NodePath) var prog_path: NodePath
onready var prog: Prog = get_node(prog_path) as Prog

export(NodePath) var viewport_path: NodePath
onready var viewport: Viewport = get_node(viewport_path) as Viewport

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Set floating name and adjust viewport size to fit its size
	var cont: CenterContainer = viewport.get_node("FloatingName") as CenterContainer
	var nametag: Label = cont.find_node("Name") as Label

	nametag.text = prog.display_name
	# Test
#    nametag.text = "Nm"
#    nametag.text = "Medium Name"
#    nametag.text = "ZZ"
#    nametag.text = "ZZZZZZZ"
#    nametag.text = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
#    nametag.text = "Very Very Long Name"

	call_deferred("fit_content", cont)
	return


func fit_content(cont: CenterContainer) -> void:
	var sz: Vector2 = cont.rect_size * 1.06 # Need some margin
	viewport.size = sz
	self.region_rect = Rect2(Vector2(0,0), sz)
	return