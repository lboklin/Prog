extends Spatial

export(NodePath) var prog_path: NodePath
export(NodePath) var inner_gimbal_path := NodePath("Swivel")
export(NodePath) var camera_path := NodePath("Swivel/Camera")

export(float) var max_zoom := 0.5
export(float) var min_zoom := 1.5
export(float) var max_fov := 120.0
export(float) var min_fov := 50.0
export(float) var zoom_speed := 2.0
export(float, 0.01, 10.0) var rotate_speed := 1.0


var _zoom_factor := 1.0
var _actual_zoom := 1.0
var _is_wasding := false

var _move_speed: Vector2
var _scroll_speed: float

var _rotation: Vector3
var _distance: float


onready var prog: Prog = get_node(self.prog_path)
onready var swivel: Spatial = get_node(self.inner_gimbal_path)
onready var camera: Spatial = get_node(self.camera_path)
onready var rolling := self.prog.get_node("Rolling") as Rolling
onready var jumping := self.prog.get_node("Jumping") as Jumping
onready var attacking := self.prog.get_node("Attacking") as Attacking
onready var orienting := self.prog.get_node("Orienting") as Orienting


onready var pointer := preload("res://prog/player/Pointer.tscn").instance() as Pointer


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	self.rolling.ignore_destination = true

	add_child(self.pointer)
	self.pointer.excluded_bodies = [self.prog]
	self.pointer.connect("pointer_updated", self, "_on_pointer_updated")
	self.connect("tree_exited", self, "_on_tree_exited")
	return


func _on_tree_exited():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	self.rolling.ignore_destination = false

	if is_instance_valid(self.pointer):
		self.pointer.queue_free()
	self.queue_free()
	return


func _on_pointer_updated(pos: Vector3):
	self.orienting.oriented_towards = pos
	return


func _input(ev):
	if ev is InputEventMouseMotion :
		self._move_speed = ev.relative

	elif ev is InputEventMouseButton:
		match ev.button_index:
			BUTTON_WHEEL_UP:
				self._zoom_factor -= 0.05
			BUTTON_WHEEL_DOWN:
				self._zoom_factor += 0.05
		self._zoom_factor = clamp(self._zoom_factor, self.max_zoom, self.min_zoom)

	elif ev is InputEventKey:
		if ev.scancode == KEY_SHIFT:# and not self._is_wasding:
			if ev.is_pressed():
				self.orienting.upright = false
#				self.rolling.stop()
			else:
				self.orienting.upright = true

	var from: Vector3 = self.jumping.destination if self.jumping.is_jumping \
			else Util.to_ground(self.prog.global_transform.origin)

#	var wasding := false

	if ev.is_action_pressed("jump"):
		var jump_pos := get_aim_pos(from, self.jumping.max_range)
		var lin_vel := self.prog.linear_velocity
		lin_vel.y = 0
		if self.rolling.is_rolling:
			jump_pos = from + lin_vel.normalized() * lin_vel.length()
		self.rolling.stop()
		self.jumping.queue_jump(Util.to_2d(jump_pos))

	elif ev.is_action_pressed("attack"):
		self.attacking.attack(get_aim_pos(from, self.attacking.max_range))

#	elif Input.is_action_pressed("move_left") \
#			or Input.is_action_pressed("move_right") \
#			or Input.is_action_pressed("move_up") \
#			or Input.is_action_pressed("move_down"):
#		self.rolling.stay_at_destination = false
#		if not self._is_wasding:
#			self.rolling.emit_signal("started_rolling")
#		self._is_wasding = true


func _process(delta: float) -> void:
	# Zoom
	self._actual_zoom = lerp(self._actual_zoom, self._zoom_factor, delta * self.zoom_speed)
	self.swivel.set_scale(Vector3(self._actual_zoom,self._actual_zoom,self._actual_zoom))
	var zoom_range := self.max_zoom - self.min_zoom
	var zoom_quot: float = 1.0 - ((self._actual_zoom - self.min_zoom) / zoom_range)
	get_viewport().get_camera().fov = self.min_fov + zoom_quot * (self.max_fov - self.min_fov)

	# Stay with the prog
	var pos: Vector3 = Util.to_ground(self.prog.global_transform.origin)
	var cur_pos := self.global_transform.origin
	var dist_weight := clamp(ease((pos - cur_pos).length() / 1.0, 5.0), 0.0, 1.0)
	self.global_transform.origin = lerp(cur_pos, pos, dist_weight)

	# Update rotation
	var rot_fac: float = lerp(0.75, 1.0, zoom_quot)
	_rotation.x += -_move_speed.y * delta * rotate_speed * rot_fac
	_rotation.y += -_move_speed.x * delta * rotate_speed * rot_fac
	if _rotation.x < -PI/2:
		_rotation.x = -PI/2
	if _rotation.x > PI/2:
		_rotation.x = PI/2
	_move_speed = Vector2()

	# Update distance
	_distance += _scroll_speed * delta
	if _distance < 0:
		_distance = 0
	_scroll_speed = 0
#
#	self.camera.set_identity()
	self.camera.translate_object_local(Vector3(0,0,_distance))
#	self.swivel.set_identity()
#	var t = Quat()
#	t.set_euler(_rotation);
	self.swivel.transform.basis = Basis(_rotation)

#	if self.jumping.is_jumping:
#		return

	var bs := self.swivel.global_transform.basis
	var dir := Vector3()
	var wasding := false
	if Input.is_action_pressed("move_left"):
		dir -= bs.x
		wasding = true
	if Input.is_action_pressed("move_right"):
		dir += bs.x
		wasding = true
	if Input.is_action_pressed("move_up"):
		dir -= bs.z
		wasding = true
	if Input.is_action_pressed("move_down"):
		dir += bs.z
		wasding = true


	if wasding:
		# We just started wasding
		if not self._is_wasding:
			self.rolling.stay_at_destination = false
			self.rolling.emit_signal("started_rolling")
			self._is_wasding = true

		if dir != Vector3():
			dir = dir.normalized()
			var speed := 30.0 if Input.is_key_pressed(KEY_SHIFT) else 10.0
			var motion := dir * speed

			var ang_vel := Rolling.calc_ang_vel(self.prog, dir, speed)
			self.prog.apply_torque_impulse(ang_vel * delta)
			self.prog.rset("goal_pos", pos + motion)

	# Was wasding but no more. Keep rolling while Shift is held down
	elif self._is_wasding and not Input.is_key_pressed(KEY_SHIFT):
		self.rolling.stay_at_destination = true
		self.rolling.stop()
		self._is_wasding = false


func get_aim_pos(from: Vector3, max_range: float) -> Vector3:
	if self.pointer.last_was_nothing:
		var dir := (self.pointer.get_current_direction() * Vector3(1,0,1)).normalized()
		return from + dir * max_range
	else:
		var vec := self.pointer.last_position - from
		return from + Util.clamp_length(vec, 0.0, max_range)