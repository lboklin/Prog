extends Control
class_name NameScore

onready var name_label: Label = $HSplitContainer/Name as Label
onready var score_label: Label = $HSplitContainer/Score as Label
onready var default_color: Color = Color(0.803922, 0.803922, 0.807843)
onready var player_color: Color = Color(0.368627, 0.792157, 0.494118)
onready var dead_player_color: Color = Color(0.8125, 0.495415, 0.371338)
onready var dead_color: Color = Color(0.820312, 0.285187, 0.285187)

var entry_name: String
var entry_score: int = 0
var is_player: bool = false

func _ready() -> void:
	name_label.text = entry_name
	score_label.text = str(entry_score)
	return

func set_name_score(entry_name_: String, entry_score_: int) -> void:
	entry_name = entry_name_
	entry_score = entry_score_
	if name_label:
		score_label.text = str(entry_score_)
		if self.is_player:
			set_color(name_label, player_color)
		else:
			set_color(name_label, default_color)
	return

func set_color(label: Label, color: Color) -> void:
	label.set("custom_colors/font_color", color)
	return

func set_dead(is_dead: bool = true) -> void:
	var color := dead_color if is_dead && not is_player \
		else dead_player_color if is_dead && is_player \
		else player_color if is_player \
		else default_color
	set_color(name_label, color)
	return