class_name Jump


signal jump_finished(at)


const G = 9.8

# Total time of any jump in seconds
const JUMP_TIME: float = 0.9


# Where the jump started from
var from := Vector2()

# Jump destination
var to := Vector2()


var _landed := false


func _init(jump_from: Vector2, jump_to: Vector2) -> void:
	self.from = jump_from
	self.to = jump_to
	return


func _on_landed(_surface: Node, state: RigidBody) -> void:
	# Make sure we've not already landed and that we're actually landing and
	# not just colliding with something
	if not _landed and state and state.linear_velocity.y <= 0:
		emit_signal("jump_finished", to)
		_landed = true
		self.call_deferred("free")
	return

var deferred := false
func jump(body: RigidBody, state: PhysicsDirectBodyState) -> bool:
	# Execute next frame
	if not deferred:
		deferred = true
		return false
#	elif state.linear_velocity.length() < 0.5:
	else:
		body.connect("body_entered", self, "_on_landed", [state])
		var vec := self.to - Util.to_2d(state.center_of_mass)
		var init_vel := Kinematics.projectile_inital(vec, JUMP_TIME)
		assert(state.total_linear_damp <= 0) # won't land where we intend if we have drag

		var counter := state.linear_velocity
		state.apply_central_impulse((init_vel - counter) * state.inverse_mass)
#		state.apply_central_impulse((init_vel) * state.inverse_mass)
		return true
#	else:
#		return false