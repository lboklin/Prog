extends Component
class_name Orienting


export(NodePath) var jumping_path := NodePath("../Jumping")
export(NodePath) var attacking_path := NodePath("../Attacking")
export(NodePath) var rolling_path := NodePath("../Rolling")


#export(float) var turn_speed := 5.0
#export(float) var attack_turn_speed := 8.0
export(float, 0.0,30.0) var balancing_power := 5.0
export(float, 0.0,30.0) var turning_power := 15.0
export var auto_balance := true
export var auto_turn := true
export var always_follow_pointer := false


var oriented_towards := Vector3()
var upright := true


var _temp_disabled := false setget set_temp_disabled
func set_temp_disabled(val):
	_temp_disabled = val
	return


onready var jumping := get_node(jumping_path) as Jumping
onready var attacking := get_node(attacking_path) as Attacking
onready var rolling := get_node(rolling_path) as Rolling


func _ready() -> void:
	self.rolling.connect("started_rolling", self, "_on_started_rolling")
	self.rolling.connect("stopped_rolling", self, "_on_stopped_rolling")
	return


func _on_started_rolling() -> void:
#	self._temp_disabled = true
	return


func _on_stopped_rolling() -> void:
	self._temp_disabled = false
	return


func _physics_process(_delta: float) -> void:
	if is_network_master() \
			and not self._temp_disabled \
			and (self.auto_balance or self.auto_turn):
#		# Negate undesired angular velocity
#		self.prog.apply_torque_impulse(-self.prog.angular_velocity * _delta)

		# Use this basis to ignore the technical orientation of our z and y
		# axes, since they're unimportant in respect to our shape and purpose.
		var sym_bs := Prog.radial_symmetry(self.prog.global_transform.basis)

		if self.auto_balance:
			var balance_tq := self.balancing_power * balance(sym_bs, upright).get_euler()
			self.prog.apply_torque_impulse((balance_tq - self.prog.angular_velocity * Vector3(1,0,1)) * _delta)

		# Turn unless we're rather horizontal
		if self.auto_turn and 0.5 < sym_bs.y.dot(Vector3.UP):
			var towards := _choose_orientation()
			var tf := Transform(sym_bs, self.prog.global_transform.origin)
			var turn_tq := self.turning_power * turn(tf, towards).get_euler()
			self.prog.apply_torque_impulse((turn_tq - self.prog.angular_velocity * Vector3.UP) * _delta)
		return
	else:
		return


func _choose_orientation() -> Vector3:
	if self.attacking.has_target():
		var wpns := self.attacking.get_by_last_activated()
		var wpn1: BeamWeapon = wpns[0]
		var wpn2: BeamWeapon = wpns[1]
		var prog_pos := self.prog.global_transform.origin
		match [wpn1.attacking_pos, wpn2.attacking_pos]:
			[Vector3(), Vector3()]:
				return self.oriented_towards
			[_, Vector3()]:
				var side := wpn1.global_transform.origin - prog_pos
				return self._angle_away(wpn1.attacking_pos, side)
			[Vector3(), _]:
				var side := wpn2.global_transform.origin - prog_pos
				return self._angle_away(wpn2.attacking_pos, side)
			_:
				return self.attacking.get_general_attack_position()
	elif self.jumping.is_jumping and not self.always_follow_pointer:
		return self.jumping.destination
	elif self.rolling.is_rolling and not self.always_follow_pointer:
		return self.rolling.rolling_to
	else:
		return self.oriented_towards


# Angle away from from, towards desired adjacent side (basis.x or -basis.x)
func _angle_away(from: Vector3, side: Vector3) -> Vector3:
	var prog_pos := self.prog.global_transform.origin
	var closest_fw := self.prog.get_alignment_closest_to(from)
	var closest_side := self.prog.get_side_closest_to(from)

	var dir := (from - prog_pos).normalized()
	var nearly_orthogonal := 0.3 > abs(closest_fw.dot(dir))
	# To stop jittering (I don't understand why it happens),
	# when closest_fw is nearly orthogonal we just use global up.
	var up := Vector3.UP if nearly_orthogonal \
			else closest_fw.cross(closest_side).normalized() #\

	var is_same_side := 0 < side.dot(closest_side)
	var same_side := Util.int_bool(is_same_side)

	# ----- Debug
#	self.prog.set_dir_indicator_dir(closest_fw)
#	self.prog.set_dir_indicator_dir(closest_side)
#	self.prog.set_dir_indicator_dir(_up)
#	self.prog.set_dir_indicator_dir(vec)
	# -----

	var ang := deg2rad(-20) * same_side
	return prog_pos + dir.rotated(up, ang)


# Lateral balance (stay upright)
static func balance(basis: Basis, towards_upright := true) -> Quat:
	var up := Vector3.UP
	if not towards_upright:
		var other_side := Util.int_bool(basis.x.angle_to(Vector3.UP) > PI / 2)
		up = basis.x * Vector3(1,0,1) * other_side
		up = up.normalized()

	var cur_rot := basis.get_rotation_quat()
	var goal_rot: Quat = Transform(basis, Vector3()).looking_at(-basis.z, up).basis.get_rotation_quat()
	return goal_rot * cur_rot.inverse()


# Turn to face oriented_towards
static func turn(tf: Transform, towards: Vector3, up := Vector3.UP) -> Quat:
	var vec := (towards - tf.origin) * Vector3(1,0,1)

	# Aligning backwards or forwards depending on what's closest
	var turned_around: int = (1 if sign(tf.basis.z.dot(vec)) > 0 else -1)
	var l_r_yaw := sign((tf.basis.x * turned_around).dot(vec))
	var look_angle: float = (tf.basis.z * turned_around).angle_to(vec)
	return Quat(up, look_angle * l_r_yaw)