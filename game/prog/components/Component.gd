extends Node
class_name Component


signal activated
signal deactivated


export(NodePath) var prog_path: NodePath = "../"
export(bool) var disabled := false setget set_disabled
func set_disabled(val):
	self.set_process(not val)
	self.set_physics_process(not val)
	disabled = val
	return


var prog: Prog


func _ready() -> void:
	self.prog = get_node(self.prog_path) as Prog
	self.set_process(not self.disabled)
	self.set_physics_process(not self.disabled)
	return

func activate():
	emit_signal("activated")

func deactivate():
	emit_signal("deactivated")