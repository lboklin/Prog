extends Component
class_name Facing


export(NodePath) var side_left_path
export(NodePath) var side_right_path
export(NodePath) var attacking_path
export(NodePath) var rolling_path
export(NodePath) var jumping_path

export(float) onready var turn_speed := 5.0
export(float) onready var attack_turn_speed := 8.0


var _facing := Vector3()


onready var look_pos := Util.to_3d(Util.rand_loc(Util.to_2d(self._facing), 1, 5))
onready var jumping := get_node(jumping_path) as Jumping

onready var _side_left := get_node(side_left_path) as Side
onready var _side_right := get_node(side_right_path) as Side
onready var _attacking := get_node(attacking_path) as Attacking
onready var _rolling := get_node(rolling_path) as Rolling


func _physics_process(delta: float) -> void:
	if is_network_master():
		var atk_pos := self._attacking.get_general_attack_position()
		var has_tgt := atk_pos != Vector3()

		# Don't try to orient ourselves when _rolling
		if self.rotating_body.linear_velocity.length() > 0.2:
			self._facing = Vector3()
		elif has_tgt:
			self._facing = atk_pos
		elif self.jumping.is_jumping:
			self._facing = Util.to_3d_ground(self.jumping.current_jump.to)
		else:
			self._facing = self.look_pos
#		self._facing = self.look_pos
		var speed = self.attack_turn_speed if has_tgt else self.turn_speed
		if self._facing != Vector3():
			rpc("look_towards_pos", self._facing, speed, delta)
	return


remotesync func look_towards_pos(pos: Vector3, _speed: float, _step: float) -> void:
	var from: Vector3 = self.rotating_body.global_transform.origin
	if pos != from:
		self.rotating_body.looking_pos = pos