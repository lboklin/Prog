extends Component
class_name Jumping


signal just_landed(at, has_queued)
signal just_jumped(to)


# How far you can jump from any given starting pos (meters)
export(float) var max_range := 10.0


# The time until you hit the ground. Useful for bots to determine their response.
var current_jump: Jump
var queued_jump: Jump
var is_jumping: bool = false
var destination := Vector3() setget ,get_destination
func get_destination() -> Vector3:
	return Util.to_3d_ground(self.queued_jump.to) if self.queued_jump \
			else Util.to_3d_ground(self.current_jump.to) if self.current_jump \
			else Vector3()


func _ready() -> void:
	self.prog.connect("player_alive", self, "_on_respawn")
	self.prog.connect("incapacitated", self, "_on_incapacitated")
	self.connect("just_landed", self, "_on_landed")
	self.prog.connect("integrate_forces", self, "_on_prog_integrate_forces", [self.prog])


func _on_landed(at: Vector3, _has_queued: bool):
	assert(Util.to_2d(at) == self.current_jump.to)
	self.prog.rset("goal_pos", at)
	self.current_jump = self.queued_jump
	self.queued_jump = null
	self.is_jumping = false
	return


func _on_respawn() -> void:
	self.current_jump = null
	self.queued_jump = null
	return


func _on_incapacitated() -> void:
	# Only remove any queued jump since it wouldn't make sense to stop
	# mid-air just because we're disabled.
	self.queued_jump = null
	return


func _on_prog_integrate_forces(state, body):
	var is_on_floor := self.prog.is_on_floor()
	if self.current_jump:
		if not self.is_jumping and is_on_floor:
			start_jump(body, state)
		elif self.is_jumping and is_on_floor and self.prog.linear_velocity.y <= 0:
			emit_signal("just_landed", \
					Util.to_3d_ground(self.current_jump.to), \
					self.queued_jump != null)

	return


func start_jump(body, state) -> void:
	if !self.current_jump && self.queued_jump:
		self.current_jump = self.queued_jump
		self.queued_jump = null

	assert(self.current_jump)
	if self.current_jump.jump(body, state):
		self.is_jumping = true

		# Break invulnerability if there is any
		if !self.prog.is_vulnerable():
			Util.expire_timer(self.prog.invulnerability_timer)

		var to := Util.to_3d_ground(self.current_jump.to)
		self.prog.rset("goal_pos", to)
		emit_signal("just_jumped", to)
	return


func queue_jump(jump_to: Vector2) -> void:
	if self.prog.is_incapacitated():
		return

	var from := self.current_jump.to if self.current_jump \
		else Util.to_2d(self.prog.puppet_pos)
	var vec: Vector2 = jump_to - from
	var to = from + Util.clamp_length_2d(vec, 0, self.max_range)
	var new_jump: Jump = Jump.new(from, to)
	if self.current_jump and self.is_jumping:
		self.queued_jump = new_jump
	else:
		self.current_jump = new_jump
		self.queued_jump = null
	self.destination = Util.to_3d_ground(jump_to)
	return