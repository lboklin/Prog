extends Component
class_name Rolling


signal stopped_rolling()
signal started_rolling()


const ALLOWED_MOVEMENT := 0.3


export(NodePath) var jumping_path := NodePath("../Jumping")
export(float, 10, 1000) var max_speed := 50.0
export(bool) var stay_at_destination := true
export(bool) var counter_roll_during_jump := false
export(float, 0.01, 3.0) var destination_deadzone := 0.1


var is_rolling: bool = false
var rolling_to := Vector3()
var ignore_destination := false


onready var jumping := get_node(jumping_path) as Jumping


func _ready() -> void:
	self.prog.connect("player_alive", self, "stop")
	self.prog.connect("incapacitated", self, "stop")
	self.jumping.connect("just_landed", self, "_on_landed")
	self.jumping.connect("just_jumped", self, "_on_jumped")
	self.connect("started_rolling", self, "_on_started_rolling")
	self.connect("stopped_rolling", self, "_on_stopped_rolling")


func _on_stopped_rolling():
	self.is_rolling = false
#	stop()
	return


func _on_started_rolling():
	self.is_rolling = true
	return


func _on_landed(_at: Vector3, has_queued: bool) -> void:
	if has_queued:
		stop()
	elif self.rolling_to == Vector3():
		roll_to(self.prog.goal_pos)
		roll_to(_at)
	return


func _on_jumped(_to) -> void:
#	stop()
	return


func _physics_process(_delta: float) -> void:
	var tf: Transform = self.prog.global_transform
	var lin_vel := self.prog.linear_velocity * Vector3(1,0,1)
	var speed := lin_vel.length()
	var is_moving := ALLOWED_MOVEMENT < speed
	var frct := self.prog.physics_material_override.friction

	# Counter roll while jumping to stop faster on landing
	if self.counter_roll_during_jump and self.jumping.is_jumping:
		var dist := ((self.rolling_to - tf.origin) * Vector3(1,0,1)).length()
		var counter := -lin_vel / max(0.01, dist)
		var ang_vel := calc_ang_vel(self.prog, counter.normalized(), counter.length() * frct)
		self.prog.apply_torque_impulse(ang_vel * _delta)
	elif not self.jumping.is_jumping:
		var dest := self.rolling_to
		if not self.ignore_destination and dest != Vector3():
			var vec: Vector3 = (dest - tf.origin) * Vector3(1,0,1)
			var dist := vec.length()

			var target_speed := calc_target_speed(speed, self.max_speed, dist, frct)
			var ang_vel := calc_ang_vel(self.prog, vec.normalized(), target_speed * 2)

			self.prog.apply_torque_impulse(ang_vel * _delta)

			var at_rest := not is_moving and dist < self.destination_deadzone
			if self.is_rolling and at_rest:
				stop()
				emit_signal("stopped_rolling")
		elif self.stay_at_destination and is_moving:
			# Make sure we stay still when we have nowhere else to be.
			var dir := lin_vel.normalized()
			var ang_vel := calc_ang_vel(self.prog, dir, 0.0)
			self.prog.apply_torque_impulse(ang_vel * _delta)
			self.prog.rset("goal_pos", tf.origin)
			if self.is_rolling:
				emit_signal("stopped_rolling")


func stop() -> void:
	self.roll_to(Vector3())
	return


func roll_to(pos: Vector3) -> void:
	if self.rolling_to == pos:
		return
	else:
		self.rolling_to = pos
		if pos == Vector3():
			if self.is_rolling:
				self.prog.rset("goal_pos", self.prog.global_transform.origin)
		elif not self.is_rolling and not self.jumping.is_jumping:
				emit_signal("started_rolling")
		self.prog.rset("goal_pos", pos)
		return


static func calc_target_speed(
		cur_speed: float,
		top_speed: float,
		dist: float,
		friction := 1.0
		) -> float:
	if dist < 0.1:
		return 0.0
	else:
		var time_to_dest := 0.5
		var weight := min(0.5 * friction, 1.0)
		var target_speed := min(dist / time_to_dest, top_speed)
		return lerp(cur_speed, target_speed, weight)


static func calc_ang_vel(
		body: RigidBody,
		dir: Vector3,
		desired_speed: float
		) -> Vector3:
	var radius := 0.375

	var desired_lin_vel := dir * desired_speed
	var desired_ang_vel := Sphere.linear2angular(desired_lin_vel, radius)
	return desired_ang_vel - body.angular_velocity