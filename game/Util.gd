
class_name Util


const GROUND_HEIGHT: float = 0.5


static func or_default(nullable, default):
	return nullable if nullable else default


static func int_bool(b: bool) -> int:
	return 1 if b else -1


static func new_timer(parent: Node, timer_name) -> Timer:
	var timer = Timer.new()
	timer.name = timer_name
	timer.one_shot = true
	parent.add_child(timer)
	return timer

static func expire_timer(timer: Timer) -> void:
	timer.stop()
	timer.emit_signal("timeout")


static func rand_color():
	return Color(
		rand_range(0, 1.0),
		rand_range(0, 1.0),
		rand_range(0, 1.0))


# New rotation for our 3D model around the global up axis
static func smooth_rotate(
		spatial: Spatial,
		point: Vector2,
		step: float,
		speed: float = 1.0,
		up: Vector3 = Vector3(0,1,0)
		) -> void:
	var current_pos := to_2d(spatial.global_transform.origin)
	var current_rot: float = \
		(spatial.rotation * up).length() if spatial.rotation != Vector3() \
		else 0.0
	var final_rot: float = (point - current_pos).rotated(PI/2).angle() * -1

	# Always count rotation in the positive
	while final_rot < 0:
		final_rot += 2*PI
	while current_rot < 0:
		current_rot += 2*PI

	var d_angle := final_rot - current_rot

	var s := sign(d_angle)
	d_angle = abs(d_angle)

	# Don't go the long way around
	if d_angle > PI:
		s *= -1
		d_angle = 2*PI - d_angle

	var rot_speed := speed
	var min_rot_speed := 0.001 * speed

	var smooth_rot_speed := step * rot_speed * d_angle# * d_angle
	var d_rot := s * min(d_angle, max(smooth_rot_speed, min_rot_speed))
	var smooth_rot := current_rot + d_rot

	# Don't keep inflating the rot value
	if smooth_rot > 2*PI:
		smooth_rot -= 2*PI
	elif smooth_rot < 0:
		smooth_rot += 2*PI

	spatial.rotation.y = smooth_rot
	return


static func global_position_2d(spatial: Spatial) -> Vector2:
	return to_2d(spatial.global_transform.origin)


# Get a random location inside a cut-out circle defined
# by a min and max of a radius from the given origin.
static func rand_loc(location: Vector2, radius_min: float, radius_max: float) -> Vector2:
	randomize() # generate new random seed or we might get the same result as previous time
	var new_radius := rand_range(radius_min, radius_max)
	var angle := deg2rad(rand_range(0, 360))
	var point_on_circ := Vector2(new_radius, 0).rotated(angle)
	return (location + point_on_circ)


static func to_screen_vec(vec: Vector3, ppm: Vector2) -> Vector2:
	var rot: float = PI/4
#		var screen_motion := (cam_motion * ppm * 2).rotated(rot)
	var screen_motion := to_2d(vec).rotated(rot) * ppm
#		var screen_motion := cam_motion * ppm
#		screen_motion.y *= 0.25
	return screen_motion * 1.15


static func move(spatial: Spatial, motion: Vector2) -> void:
	var motion_3d := Vector3(motion.x, 0.0, motion.y)
	spatial.global_transform.origin += motion_3d


static func to_3d(pos: Vector2) -> Vector3:
	return Vector3(pos.x, 0, pos.y)


static func to_3d_ground(pos: Vector2) -> Vector3:
	return to_ground(to_3d(pos))


static func to_2d(pos: Vector3) -> Vector2:
	return Vector2(pos.x, pos.z)


static func to_ground(pos: Vector3) -> Vector3:
	return pos * Vector3(1,0,1) + Vector3(0,GROUND_HEIGHT,0)


static func to_horizontal(vec: Vector3) -> Vector3:
	return (vec * Vector3(1,0,1)).normalized() * vec.length()


static func clamp_length_2d(vec: Vector2, min_length: float, max_length: float) -> Vector2:
	var v3 = clamp_length(Vector3(vec.x, 0, vec.y), min_length, max_length)
	return Vector2(v3.x, v3.z)


static func clamp_length(vec: Vector3, min_length: float, max_length: float) -> Vector3:
	var vec_dir := vec.normalized()
	return vec_dir * clamp(vec.length(), min_length, max_length)
#	var max_vec: Vector3 = max_length * vec_dir
#	var min_vec: Vector3 = min_length * vec_dir
#	var vec_length := vec.length()
#	return min_vec if vec_length < min_length \
#		else max_vec if vec_length > max_length \
#		else vec


static func rand_gaussian(mean: float = 0.0, std_dev: float = 1.0) -> float:
	var u: float
	var v: float

	var s: float = 0.0
	while s >= 1.0 || s == 0.0:
		u = 2.0 * rand_range(0.0, 1.0) - 1.0
		v = 2.0 * rand_range(0.0, 1.0) - 1.0
		s = u * u + v * v

	return mean + std_dev * u * sqrt((-2.0 * log(s)) / s)
