extends Node
class_name GameRound


export(NodePath) var lobby_path
onready var lobby := get_node(lobby_path)

export(NodePath) var game_settings_path
onready var game_settings: GameSettings = get_node(game_settings_path)

export(NodePath) var players_parent_path
onready var players_parent: Spatial = get_node(players_parent_path)


enum Status { ALIVE, DEAD, GONE }

signal score_updated()
signal status_updated()


func _ready() -> void:
	if get_tree().is_network_server():
		connect("score_updated", self, "_score_updated")
		connect("status_updated", self, "_status_updated")
	return


remotesync var scorekeeper := {} # Dict of player name => player_score (int)
remotesync var statuskeeper := {} # Dict of player name => Status


func _on_tree_changed():
	for p in players_parent.get_children():
		if p is Prog:
			var p_name: String = p.get_player_name()
			if not (statuskeeper.has(p_name) || scorekeeper.has(p_name)):
				add_to_keepers(p, p_name)



var round_timer: float = 0
var timer_point_reward: float = 0
func _process(delta: float) -> void:
	if get_tree().is_network_server():
		round_timer += delta

		timer_point_reward += delta
		if game_settings.point_reward_rate > 0 \
				&& timer_point_reward > game_settings.point_reward_rate:
			add_points("all", 1) # Award everyone one point for staying alive
			timer_point_reward = 0
	return


# Only run on the server
func _score_updated(player_name: String, score: int) -> void:
	if get_tree().is_network_server():
		scorekeeper[player_name] = score
		rset("scorekeeper", scorekeeper)
		var prog: Prog = get_prog(player_name)
		if score >= 5:
			prog.rset("can_jump_while_attacking", true)
		if score >= 15:
			prog.rset("can_attack_while_jumping", true)
	return


# If this is called on server, update status.
func _status_updated(player_name: String, status: int) -> void:
	assert(get_tree().is_network_server())
	statuskeeper[player_name] = status
	rset("statuskeeper", statuskeeper)
#	var prog: Prog = get_prog(player_name)
	match status:
		Status.ALIVE:
			pass #prog.rpc("respawn", Vector3())
		Status.DEAD:
			pass
#			prog.rpc("die", game_settings.default_respawn_time)
		Status.GONE:
			scorekeeper.erase(player_name)
			statuskeeper.erase(player_name)
	return


func get_prog(player_name: String) -> Prog:
	# If it's a me
	if player_name == lobby.my_data.player_name:
		return players_parent.get_node(str(get_tree().get_network_unique_id())) as Prog
	else:
		# If it's one of our peers
		for id in lobby.peers_id_data.keys():
			var data = lobby.peers_id_data.get(id)
			if data && player_name == data.player_name:
				return players_parent.get_node(str(id)) as Prog
		# otherwise it's (relatively) safe to say it's a bot
		var bot_prog: Prog = players_parent.get_node(player_name) as Prog
		assert(bot_prog)
		return bot_prog


func add_to_keepers(prog: Prog, player_name: String) -> void:
	# Award points to the killer upon the death of their target
	if not prog.is_connected("player_killed", self, "_player_killed"):
		prog.connect("player_killed", self, "_player_killed", [prog.display_name])
	if not prog.is_connected("player_alive", self, "_player_alive"):
		prog.connect("player_alive", self, "_player_alive", [prog.display_name])

	statuskeeper[player_name] = Status.ALIVE
	scorekeeper[player_name] = 0
	return


# This will get called on everyone
func _player_alive(player: String) -> void:
	emit_signal("status_updated", player, Status.ALIVE)
	return


# This will get called on everyone
func _player_killed(killer: String, player: String) -> void:
	emit_signal("status_updated", player, Status.DEAD)
	add_points(killer, game_settings.kill_reward)
	# Reset score and powerups
	var prog: Prog = get_prog(player)
	prog.rset("can_jump_while_attacking", false)
	prog.rset("can_attack_while_jumping", false)
	rpc("emit_score_updated", player, 0)
	return


# Server assigns player (ID) additional points. If player_name is empty,
# nobody gets points, if player is "all" all players are given the points.
func add_points(player_name: String, points: int) -> void:
	if not get_tree().is_network_server():
		return

	match player_name:
		"all": # Everyone gets points
			for p in players_parent.get_children():
				if p is Prog:
					var p_name: String = p.get_player_name()
					# but only if they're alive
					if statuskeeper.get(p_name) == Status.ALIVE:
						var score: int = scorekeeper.get(p_name, 0) + points
						rpc("emit_score_updated", p_name, score)
			return
		"": # Nobody gets anything
			return
		_: # Give points to player
			if not statuskeeper.has(player_name):
				statuskeeper[player_name] = Status.ALIVE
			var score: int = scorekeeper.get(player_name, 0) + points
			rpc("emit_score_updated", player_name, score)
			return


remotesync func emit_score_updated(who: String, new_score: int) -> void:
	emit_signal("score_updated", who, new_score)
	return


func spawn_prog(id: int, player_name: String, id_as_node_name: bool = true) -> Prog:
	var prog: Prog = (preload("res://prog/Prog.tscn") as PackedScene)\
		.instance() as Prog
	prog.set_name(str(id))
	prog.set_network_master(id)
	prog.display_name = player_name
	if not id_as_node_name:
		prog.name = player_name
	self.players_parent.add_child(prog)
	if self.game_settings.show_own_nametag:
		var nametag := (preload("res://prog/Nametag.tscn") as PackedScene).instance()
		nametag.prog_path = prog.get_path()
		self.players_parent.add_child(nametag)
	var display_name: String = player_name if player_name != "" else prog.name
	add_to_keepers(prog, display_name)
	return prog


# Called on server - player with owner_id requests a bot to be spawned
# and owner_id be made master of it.
remotesync func new_bot(owner_id: int):
	if get_tree().is_network_server():
		var bot_name: String = generate_bot_name()
		if bot_name != "":
			rpc("add_bot", owner_id, bot_name)
	return


# Called on everyone
remotesync func add_bot(owner_id: int, bot_name: String):
	var is_peer: bool = owner_id in lobby.peers_id_data
	var known_bots: PoolStringArray = \
		lobby.peers_id_data[owner_id].spawned_bots if is_peer \
		else lobby.my_data.spawned_bots
	# If this is a request to add a new bot we didn't know about
	if not (bot_name in known_bots):
		known_bots.append(bot_name)
		if is_peer:
			lobby.peers_id_data[owner_id].spawned_bots = known_bots
		else:
			lobby.my_data.spawned_bots = known_bots
	spawn_bot(owner_id, bot_name)
	print("Bot ", bot_name, " has joined the party")
	return


func spawn_bot(owner_id: int, display_name: String):
	var bot_prog: Prog = spawn_prog(owner_id, display_name, false)
	var bot := (preload("res://prog/Bot.tscn") as PackedScene)\
		.instance() as Bot
#    bot.accuracy = 0.8
	bot.prog_path = bot_prog.get_path()
	bot.jumping_path = bot_prog.get_node("Jumping").get_path()
	bot.attacking_path = bot_prog.get_node("Attacking").get_path()
	bot.orienting_path = bot_prog.get_node("Orienting").get_path()
	bot.set_network_master(owner_id)
	bot_prog.add_child(bot)

	add_to_keepers(bot_prog, display_name)
	return bot_prog


func spawn_player(prog: Prog, player_id: int) -> Player:
	var player: Player = \
		(preload("res://prog/player/Player.tscn") as PackedScene)\
		.instance() as Player
	player.prog_path = prog.get_path()
	self.players_parent.add_child(player)

	player.set_network_master(player_id)
	return player


func generate_bot_name() -> String:
	var bots_available: int = available_bot_names.size()
	if bots_available > 0:
		var bot_i: int = randi() % (bots_available - 1) if bots_available > 1 \
			else 0
		var display_name: String = available_bot_names[bot_i]
		available_bot_names.remove(bot_i)
		taken_bot_names.append(display_name)
		return display_name
	else:
		print("All bot names are already taken")
		return ""

var taken_bot_names: PoolStringArray = PoolStringArray()
var available_bot_names: PoolStringArray = PoolStringArray([
	"Jack",
	"Anne",
	"Flint",
	"Edward",
	"Vane",
	"Eleanor",
	"Silver",
	"Bones",
	"De Groot",
	"Joji",
	"Mr Scott",
	"Featherstone",
	"Hornigold",
	"Miranda",
	"Thomas"
	])
