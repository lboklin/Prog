extends Position3D
class_name Pointer


signal pointer_updated(pos)


var last_position := Vector3()
var last_was_nothing := true
var update_continuously := true
var excluded_bodies := []


var _screen_pos := Vector2() setget force_screen_position
var _forced_screen_pos := false
func force_screen_position(val):
	_screen_pos = val
	_forced_screen_pos = val != Vector2()
	return


var _perform_update := false
func queue_update() -> void:
	_perform_update = true
	return


func _ready() -> void:
	self.connect("tree_exited", self, "_on_tree_exited")
	return


func _on_tree_exited():
	self.queue_free()
	return


func _physics_process(_delta: float) -> void:
	if self.update_continuously or self._perform_update:
		var space_state := self.get_world().direct_space_state
		var vp := self.get_viewport()
		var pos = get_pointer_pos(vp, space_state, self.excluded_bodies) \
				if not self._forced_screen_pos \
				else pos_from_screen_pos(
						self._screen_pos,
						vp.get_camera(),
						space_state,
						self.excluded_bodies,
						null)

		# pos is null if we're at _screen_pos and there's nothing there
		if pos != null:
			self.global_transform.origin = pos
			self.last_position = pos
			self.last_was_nothing = false
			self.emit_signal("pointer_updated", pos)
		else:
			self.last_was_nothing = true
	return


func get_current_direction() -> Vector3:
	var vp := get_viewport()
	return vp.get_camera().project_ray_normal(vp.get_mouse_position())


static func get_pointer_pos(
		vp: Viewport,
		space_state: PhysicsDirectSpaceState,
		excs := [],
		default=null) \
		-> Vector3:
	var result: Dictionary = get_pointer_collision(vp, space_state, excs)
	if not result.empty():
		return result["position"]
	else:
		return default


# Pointer ray collider intersection in global coordinates
static func get_pointer_collision(
		vp: Viewport,
		space_state: PhysicsDirectSpaceState,
		excs := []) \
		-> Dictionary:
	var mouse_pos: Vector2 = vp.get_mouse_position()
	return collision_from_screen_pos(mouse_pos, vp.get_camera(), space_state, excs)


static func pos_from_screen_pos(
		pos: Vector2,
		camera: Camera,
		space_state: PhysicsDirectSpaceState,
		excs := [],
		default=null) \
		-> Vector3:
	var result: Dictionary = collision_from_screen_pos(pos, camera, space_state, excs)
	if not result.empty():
		return result["position"]
	else:
		return default


static func collision_from_screen_pos(
		pos: Vector2,
		camera: Camera,
		space_state: PhysicsDirectSpaceState,
		excs := []) \
		-> Dictionary:
	var ray_length: int = 1000
	var from: Vector3 = camera.project_ray_origin(pos)
	var to: Vector3 = from + camera.project_ray_normal(pos) * ray_length
	return space_state.intersect_ray(from, to, excs)