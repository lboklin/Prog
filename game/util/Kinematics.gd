class_name Kinematics


static func height_at(t: float, t_total: float, gravity: float) -> float:
	# https://courses.lumenlearning.com/boundless-physics/chapter/projectile-motion/
	# d_y is vertical displacement (height)
	# v_0y is initial vertical velocity
	# in
	# d_y = v_0y * t * sin(theta) - G * t^2 / 2
	# when t = t_total and d_y = 0 (i.e. we land):
	# 0 = v_0y * t_total * sin(theta) - G * t_total^2 / 2
	# v_0y * t_total * sin(theta) = G * t_total^2 / 2
	# v_0y * sin(theta) = G * t_total / 2
	# v_0y = G * t_total / (2 * sin(theta))

	# We are able to "cheat" our way out of not knowing
	# the initial angle by saying it's 90 degrees (jumping in place)
	# theta = PI/2
	# v_0y = G * t_total / 2 * sin(theta)
	# d_y = v_0y * t * sin(theta) - G * t^2 / 2
	var v_0y = gravity * t_total / 2
	var d_y = v_0y * t - gravity * t * t / 2
	return max(0, d_y)


static func projectile_inital(\
		path: Vector2, \
		total_time: float, \
		gravity: float = 9.8) \
		-> Vector3:
	# Initial vertical velocity formula:
	# v0_y = gravity * total_time / (2 * sin(inital_angle))
	# We can treat the vertical and horizontal components separately and
	# as such we only care about the vertical motion and pretend we simply
	# shoot straight up.
	var init_angle: float = PI/2

	# we end up with an initial vertical velocity of
	var v0_y: float = gravity * total_time / (2 * sin(init_angle))

	# Initial horizontal velocity (ground vector over total_time):
	var v0_x := path / total_time
#	print(v0_x)
#	print(current_vel)
	return Vector3(v0_x.x, v0_y, v0_x.y)