class_name Sphere


static func speed2angle(speed: float, radius: float) -> float:
#	var circumference := 2 * PI * radius
#	return (2 * PI * speed) / circumference
	return speed / radius

static func angle2speed(angle: float, radius: float) -> float:
	return angle * radius

static func linear2angular(
		lin_vel: Vector3,
		radius: float,
		up := Vector3(0,1,0)
		) -> Vector3:
	var angle := speed2angle(lin_vel.length(), radius)
	var axis := lin_vel.cross(-up).normalized()
	return axis * angle

static func angular2linear(
		ang_vel: Vector3,
		radius: float,
		up := Vector3(0,1,0)
		) -> Vector3:
	var speed := angle2speed(ang_vel.length(), radius)
	var dir := ang_vel.cross(up).normalized()
	return dir * speed