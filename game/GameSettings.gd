extends Node
class_name GameSettings

# TODO: Probably declare and emit signal for when settings are changed.
# .. after there's a settings interface of course.


var point_reward_rate: int # How many seconds alive award a point
var default_respawn_time: float
var show_own_nametag: bool
var kill_reward: int


# Set properties in _init to make error checker feel better about unused properties.
func _init() -> void:
	self.point_reward_rate = 0
	self.default_respawn_time = 3.0
	self.kill_reward = 1
	self.show_own_nametag = false
	return