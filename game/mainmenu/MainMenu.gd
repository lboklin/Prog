extends Control

# CONTAINERS
onready var menu = find_node("Menu")
onready var join_window = find_node("JoinWindow")
onready var host_window = find_node("HostWindow")

onready var light_1 = $Background/Light1
onready var light_2 = $Background/Light2

onready var window_size = get_viewport().get_visible_rect().size

const MENU_LIGHT_SPEED = 400

# Removed - set project settings editor/main_run_args instead.
#export(bool) var no_main_menu := true
#export(bool) var quick_host := true

var light_pos = Vector2(0,0)

# MAIN MENU - Join Game
# Opens up the 'Connect to Server' window
func _on_join_game_button_pressed():
	join_window.popup_centered()


# MAIN MENU - Host Game
# Opens up the 'Choose a nickname' window
func _on_host_game_button_pressed():
	host_window.set_player_name("Host")
	host_window.popup_centered()


# MAIN MENU - Quit Game
func _on_quit_game_button_pressed():
	get_tree().quit()


# JOIN CONTAINER - Connect
# Attempts to connect to the server
# Jump in game if successful
func _on_connect_button_pressed(nickname: String, ip_address: String):
	# Connect to server
	$Lobby.join_game(nickname, ip_address)
	return


# HOST CONTAINER - Continue (from choosing a nickname)
# Opens the server for connectivity from clients
func _on_host_continue_button_pressed():
	# Tuck away menu
	host_window.hide()
	self.hide()
	$CanvasLayer/Control.hide()

	# Establish network
	$Lobby.host_game(host_window.get_player_name())
	return


# ALL - Cancel (from any container)
func _on_cancel_button_pressed():
	join_window.hide()
	join_window.find_node("LabelError").set_text("")
	host_window.hide()
	host_window.find_node("LabelError").set_text("")
	# Disconnect networking
	get_tree().network_peer = null


# Handles what to happen after server ends
func _on_server_ended():
	join_window.show()
	join_window.find_node("ConnectButton").set_disabled(false)
	get_tree().network_peer = null
	print("Server ended")

	# If we are ingame, remove world from existence!
	if(has_node("/root/World")):
		(get_node("/root/MainMenu") as Control).show() # Enable main menu
		get_node("/root/World").queue_free() # Terminate world


func _on_server_error(err):
	print("Server error: ", str(err))


func _on_connection_success():
	# Tuck away menu
	self.hide() # You'd think this would hide all the children but noooo
	$CanvasLayer/Control.hide() # not even this
	join_window.hide()


func _on_connection_fail():
	join_window.show()
	# Display error telling the user that the server cannot be connected
	join_window.error_message("Cannot connect to server, try again or use another IP address")

	# Enable continue button again
	join_window.find_node("ConnectButton").set_disabled(false)


func _on_viewport_size_changed():
	window_size = get_viewport().get_visible_rect().size
	menu.set_size(window_size)


func skip_main_menu(is_connect: bool = false, ip_address: String = "localhost") -> void:
	if is_connect:
		menu.find_node("JoinGameButton").emit_signal("pressed")
		join_window.lineedit_ip_address.text = ip_address
		join_window._on_connect_pressed()
	else:
		menu.find_node("HostGameButton").emit_signal("pressed")
		host_window.emit_signal("continue_host")


# Get argument key and value as an array of one to two string elements
# depending on whether a value was provided.
func get_arg_key_val(arg_str: String) -> PoolStringArray:
	return PoolStringArray(arg_str.lstrip("--").split("=", false, 1))


func _ready():
	get_tree().connect("connection_failed", self, "_on_connection_fail")
	get_tree().connect("connected_to_server", self, "_on_connection_success")

	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")

	for arg in OS.get_cmdline_args():
		var key_val: PoolStringArray = get_arg_key_val(arg)
		match key_val[0]:
			"host": # No value expected
				call_deferred("skip_main_menu")
			"connect": # May have ip as value
				var ip_address: String = key_val[1] \
					if key_val.size() > 1 else "localhost"
				call_deferred("skip_main_menu", true, ip_address)
			_:
				print("Unsupported argument: ", arg)
				print("Supported arguments are:\n  --connect[=ip]\n  --host")
				get_tree().quit()

	return


func _process(delta):

	var margin_x = window_size.x * 0.08 * light_1.texture_scale
	var margin_y = window_size.y * 0.02 * light_1.texture_scale

	light_pos.x += delta * MENU_LIGHT_SPEED
	light_pos.y = -10
	if light_pos.x > window_size.x + margin_x:
		light_pos.x = -margin_x
	light_1.position = light_pos

	var light_2_pos = Vector2()
	light_2_pos.x = -light_pos.x + window_size.x
	light_2_pos.y = window_size.y + margin_y
	light_2.position = light_2_pos
