extends WindowDialog

signal connect_to_server
signal cancel_join

onready var lineedit_player_name := find_node("LineEditName") as LineEdit
onready var lineedit_ip_address := find_node("LineEditIPAddress") as LineEdit


func _on_about_to_show():
    lineedit_player_name.select_all()
    lineedit_player_name.grab_focus()
    return

func _on_cancel_pressed():
    hide()
    emit_signal("cancel_join")
    return

func _on_connect_pressed():
    var txt = lineedit_player_name.text
    var player_name: String = txt if txt != "" else lineedit_player_name.placeholder_text

    # Check entered IP address for errors
    var ip_address = lineedit_ip_address.get_text()
    if ip_address == "localhost": ip_address = "127.0.0.1"
    if not ip_address.is_valid_ip_address():
        error_message("Invalid IP address")
        return

    # Check player_name for errors
    if player_name == "":
        error_message("player_name cannot be empty")
        return

    # Clear error (if any)
    error_message("")

    # While we are attempting to connect, disable button for 'continue'
    (find_node("ConnectButton") as Button).set_disabled(true)

    emit_signal("connect_to_server", player_name, ip_address)
    return


func error_message(msg: String = "") -> void:
    var txt = "Error: " + msg if msg != "" else ""
    (find_node("LabelError") as Label).set_text(txt)
    return