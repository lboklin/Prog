extends WindowDialog

signal continue_host
signal cancel_host

onready var lineedit_player_name := find_node("LineEditName") as LineEdit


func _on_about_to_show() -> void:
    lineedit_player_name.select_all()
    lineedit_player_name.grab_focus()
    return

func _on_CancelButton_pressed() -> void:
    hide()
    emit_signal("cancel_host")
    return

func _on_ContinueButton_pressed() -> void:

    # Clear error (if any)
    if lineedit_player_name.text == "":
        lineedit_player_name.text = lineedit_player_name.placeholder_text
    if lineedit_player_name.text == "":
        error_message("Name cannot be empty")
        return

    error_message("")
    emit_signal("continue_host")
    return


func get_player_name() -> String:
    return lineedit_player_name.text

func set_player_name(player_name: String) -> void:
    lineedit_player_name.text = player_name
    return

func error_message(msg: String = "") -> void:
    var txt = "Error: " + msg if msg != "" else ""
    (find_node("LabelError") as Label).set_text(txt)
    return