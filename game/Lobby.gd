extends Node

export(NodePath) var join_window_path
onready var join_window := get_node(join_window_path) as WindowDialog

#export(NodePath) var host_window_path
#onready var host_window := get_node(host_window_path) as WindowDialog

export(NodePath) var main_menu_path
onready var main_menu := get_node(main_menu_path) as Control

const SERVER_PORT: int = 31041
const MAX_PLAYERS: int = 6

signal server_error()

func _ready():
	# Connect all functions
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

class PlayerData:
	var player_name: String
	var primary_color: Color
	var secondary_color: Color
	var in_game: bool
	# Bots this player has spawned
	var spawned_bots: PoolStringArray = PoolStringArray()
	func _init(name: String, \
			color1: Color = Color(), \
			color2: Color = Color(),
			bots = PoolStringArray()):
		player_name = name
		primary_color = color1
		secondary_color = color2
		spawned_bots = bots
		in_game = false
		return

# Player data for all other players, associate ID to data
var peers_id_data = {}
# Info we send to other players
onready var my_data: PlayerData = PlayerData.new("Unruly Rufus")


func _player_connected(id: int) -> void:
	print("Player connected (", str(id), ")")
	return

func _player_disconnected(id: int) -> void:
	var pname = peers_id_data[id].player_name
	print(pname, " (", str(id), ")", " has quit the game")
	var root = get_tree().get_root()
	if root.has_node("GameRound"):
		var game_round = get_node("/root/GameRound")
		var ps = game_round.get_node("Players")
		game_round.emit_signal("status_updated", pname, game_round.Status.GONE)
		if ps.has_node(str(id)):
			print("Deleting ", pname, "'s prog")
			ps.get_node(str(id)).queue_free()

	peers_id_data.erase(id) # Erase player from data.
	return

func _connected_ok() -> void:
	# Only called on clients, not server. Send my ID and data to all the other peers.
	assert(my_data)
	var my_id: int = get_tree().get_network_unique_id()
	rpc("register_player", my_id, my_data.player_name, PoolStringArray())
	return

func _server_disconnected() -> void:
	# Show main menu
	get_node("/root/GameRound").queue_free()
	main_menu.show()
	main_menu.get_node("CanvasLayer/Control").show()

	print("Server disconnected")
	join_window.show()
	join_window.find_node("ConnectButton").set_disabled(false)

	get_tree().network_peer = null
	return


func _connected_fail() -> void:
	get_tree().network_peer = null
	return # Could not even connect to server; abort.


remote func register_player(id: int, player_name: String, bots: PoolStringArray) -> void:
	# If I'm the server, let the new guy know about existing players.
	if get_tree().is_network_server():
		# Send my data to new player
		assert(my_data)
		rpc_id(id, "register_player", 1, my_data.player_name, my_data.spawned_bots)

		# Send the data of existing players
		for peer_id in peers_id_data.keys():
			var peer_data: PlayerData = peers_id_data[peer_id]
			var peer_name: String = peer_data.player_name
			rpc_id(id, "register_player", peer_id, peer_name, peer_data.spawned_bots)
			print("(As server) Registered ", peer_name)

		rpc_id(id, "all_players_registered")
	else:
		print("(As client) Registered ", player_name)

	# Store the data
	var dat = PlayerData.new(player_name)
	dat.spawned_bots = bots
	peers_id_data[id] = dat

	var peer_names = get_peer_names(peers_id_data)
	peer_names.append(my_data.player_name)
	return


# Called on client when server is done sending it other players
remote func all_players_registered():
	pre_configure_game()
	return


remote func pre_configure_game() -> void:
	# Load game round
	var game_round: GameRound = \
		(load("res://GameRound.tscn") as PackedScene)\
		.instance() as GameRound
	game_round.lobby_path = get_path()
	# Load game settings
	var game_settings: GameSettings = load("res://GameSettings.gd").new()

	# Add them both
	var root: Node = get_tree().get_root()
	root.add_child(game_settings)
	game_round.game_settings_path = game_settings.get_path()
	root.add_child(game_round)

	# Create my prog with player component
	var selfPeerID: int = get_tree().get_network_unique_id()
	var my_prog: Prog = game_round.spawn_prog(selfPeerID, my_data.player_name)
	var player_component = game_round.spawn_player(my_prog, selfPeerID)
	# Create my HUD
	var hud: HUD = \
		(load("res://prog/player/HUD.tscn") as PackedScene).instance()
	hud.game_round_path = game_round.get_path()
	hud.player_path = player_component.get_path()
	hud.set_network_master(selfPeerID)
	game_round.add_child(hud)

	# Load other players
	for id in peers_id_data.keys():
		var data: PlayerData = peers_id_data[id]
		var player = game_round.spawn_prog(id, data.player_name)
		for bot_name in data.spawned_bots:
			game_round.add_bot(id, bot_name)
		player.set_network_master(id)


	# If we're alone (as server), go straight for post_configure_game
	if peers_id_data.empty():
		assert(get_tree().is_network_server())
		post_configure_game()
	else:
		# Tell server that this peer is done pre-configuring.
		rpc_id(1, "done_preconfiguring", selfPeerID)
	return


var players_done = []
remote func done_preconfiguring(id: int) -> void:
	assert(get_tree().is_network_server()) # Running this a server
	assert(id in peers_id_data) # Exists
	assert(not id in players_done) # Was not added yet

	# Add this player
	var game_round = get_node("/root/GameRound")
	var prog = game_round.spawn_prog(id, peers_id_data[id].player_name)
	game_round.add_to_keepers(prog, peers_id_data[id].player_name)
	peers_id_data[id].in_game = true
	players_done.append(id)

	# Was the last one to become ready
	if players_done.size() == peers_id_data.size():
		rpc("post_configure_game")
	return


remotesync func post_configure_game() -> void:
	# We don't really need to do anything in here, but maybe in the future.
	# Game starts now!
	return


# Join a server
func join_game(player_name: String, ip_address: String):
	# Store own player name
	my_data = PlayerData.new(player_name, Color8(255, 125, 0))

	get_tree().network_peer = null # just in case one exists, kill it
	# Initializing the network as client
	var peer := NetworkedMultiplayerENet.new()
	var err: int = peer.create_client(ip_address, SERVER_PORT)
	if err != OK:
		print("Error creating client")
	else:
		print("Joined game")
		get_tree().network_peer = peer
	return


# Host the server
func host_game(host_player_name: String) -> void:
	# Store own player name
	my_data = PlayerData.new(host_player_name, Color8(255, 125, 0))

	get_tree().network_peer = null # just in case one exists, kill it
	# Initializing the network as server
	var peer := NetworkedMultiplayerENet.new()
	var err: int = peer.create_server(SERVER_PORT, MAX_PLAYERS)
	if err != OK:
		emit_signal("server_error", err)
	else:
		get_tree().network_peer = (peer as NetworkedMultiplayerPeer)
		print("Server creation ok")
		pre_configure_game()
	return


static func get_peer_names(id_datas: Dictionary) -> PoolStringArray:
	var peer_names: Array = []
	for peer_data in id_datas.values():
		peer_names.append(peer_data.player_name)
	peer_names.sort()
	return PoolStringArray(peer_names)


# Quits the game, will automatically tell the server you disconnected; neat.
func quit_game():
	if has_node("/root/GameRound"):
		var game_round = get_node("/root/GameRound")
		game_round.queue_free()
	get_tree().set_network_peer(null)
	get_tree().quit()