extends Node

export(NodePath) var viewport_path: NodePath
onready var viewport: Viewport = get_node(viewport_path) as Viewport

#export(float) var edge_threshold = 0.2

func _ready() -> void:
	viewport.set_vflip(true)
	$CanvasLayer/ViewportSprite.texture = viewport.get_texture()
#	$CanvasLayer/ViewportSprite.material.shader\
#		.set("shader_param/edge_threshold", edge_threshold)
	self.get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	_on_viewport_size_changed()
	return


func _on_viewport_size_changed():
	var window_size: Vector2 = self.get_viewport().get_visible_rect().size
#	self.get_viewport().size = window_size
	viewport.size = window_size
	$CanvasLayer/ViewportSprite.material.shader.set("shader_param/resolution", window_size)
	return