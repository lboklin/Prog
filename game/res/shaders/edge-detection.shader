shader_type canvas_item;

uniform vec2 resolution;
uniform float edge_threshold : hint_range(0.0, 10.0) = 0.2;
uniform float saturation : hint_range(0.0, 1.0) = 1.0;
uniform vec4 edge_color : hint_color;
uniform bool transparent = false;
uniform vec4 overlay_color : hint_color = vec4(0.7,0.7,0.7,1.0);

vec3 a(sampler2D tex, vec2 uv) {
	vec2 offset = vec2(1.0) / vec2(resolution.x, resolution.y);
	vec2 off_x = vec2(offset.x, 0.0);
	vec2 off_y = vec2(0.0, offset.y);
	vec4 edge_h = 
		texture(tex, uv - off_x) * 2.0 +
		texture(tex, uv + off_x) * -2.0 +
		texture(tex, uv - off_x - off_y) * 1.0 +
		texture(tex, uv - off_x + off_y) * -1.0 +
		texture(tex, uv + off_x - off_y) * 1.0 +
		texture(tex, uv + off_x + off_y) * -1.0;
		
	vec4 edge_v = 
		texture(tex, uv - off_x) * 2.0 +
		texture(tex, uv + off_x) * -2.0 +
		texture(tex, uv - off_x - off_y) * 1.0 +
		texture(tex, uv - off_x + off_y) * -1.0 +
		texture(tex, uv + off_x - off_y) * 1.0 +
		texture(tex, uv + off_x + off_y) * -1.0;
		
	vec3 result = sqrt(edge_h.rgb * edge_h.rgb + edge_v.rgb * edge_v.rgb);
	return result;
}

void fragment() {
	vec3 result = a(TEXTURE, UV.xy);
	float edge = length(result);
	float a = min(1.0, edge / (edge_threshold));
	vec4 pixel = texture(TEXTURE, UV.xy);
	vec4 with_overlay = mix(pixel, overlay_color, overlay_color.a);  
	vec4 with_sat = mix(vec4(0.0, 0.0, 0.0, a), with_overlay, saturation);
	vec4 with_edges = mix(with_sat, edge_color, a);
	if (transparent) { with_edges.a = a; }
	
	COLOR = with_edges;
}