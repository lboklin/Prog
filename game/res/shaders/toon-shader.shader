shader_type spatial;
//render_mode unshaded;
//render_mode depth_draw_never;

uniform sampler2D tex : hint_albedo;
uniform sampler2D normal_map : hint_albedo;
uniform vec4 color : hint_color = vec4(1.0, 1.0, 1.0, 1.0);
uniform vec4 outline_color : hint_color = vec4(0.0, 0.0, 0.0, 1.0);
uniform bool useTexture = true;
uniform bool useOutline = false;
uniform bool useNormalMap = false;
uniform float outline_thickness : hint_range(0.0,1.0) = 0.1;
uniform float outline_threshold = 0.2;
uniform float light_intensity : hint_range(0.0,1.0) = 0.8;
uniform float shadow_intensity : hint_range(0.0,1.0) = 0.8;
uniform float cut_point : hint_range(-1.0, 1.0) = 0.5;
uniform float normalMapDepth;

vec3 a(sampler2D screen_tex, vec2 uv) {
	ivec2 resolution = textureSize(screen_tex, 100);
	vec2 offset = vec2(1.0) / vec2(float(resolution.x), float(resolution.y));
	vec2 off_x = vec2(offset.x, 0.0);
	vec2 off_y = vec2(0.0, offset.y);
	vec4 edge_h = 
		texture(screen_tex, uv - off_x) * 2.0 +
		texture(screen_tex, uv + off_x) * -2.0 +
		texture(screen_tex, uv - off_x - off_y) * 1.0 +
		texture(screen_tex, uv - off_x + off_y) * -1.0 +
		texture(screen_tex, uv + off_x - off_y) * 1.0 +
		texture(screen_tex, uv + off_x + off_y) * -1.0;

	vec4 edge_v = 
		texture(screen_tex, uv - off_x) * 2.0 +
		texture(screen_tex, uv + off_x) * -2.0 +
		texture(screen_tex, uv - off_x - off_y) * 1.0 +
		texture(screen_tex, uv - off_x + off_y) * -1.0 +
		texture(screen_tex, uv + off_x - off_y) * 1.0 +
		texture(screen_tex, uv + off_x + off_y) * -1.0;

	vec3 result = sqrt(edge_h.rgb * edge_h.rgb + edge_v.rgb * edge_v.rgb);
	return result;
}

void fragment()
{
	if(useTexture)
	{
		vec3 a1 = texture(tex, UV).rgb;
		ALBEDO = a1*color.rgb;
	} else
	{
		ALBEDO = color.rgb;
	}
	if(useNormalMap == true)
	{
		vec3 normalmap = vec3(1.0,1.0,1.0) - texture(normal_map, UV).xyz * vec3(2.0,2.0,2.0);
		vec3 normal = normalize(TANGENT * normalmap.y + BINORMAL * normalmap.x + NORMAL * normalmap.z);
		NORMAL =normal;
	} else
	{
		NORMALMAP_DEPTH = 0.0;
	}
}

float calc_toonStripes(float NdotL)
{
	if(NdotL > cut_point)
	{
		return light_intensity;
	} else
	{
		return 1.0 - shadow_intensity;
	}
}

void light()
{
	float NdotL = dot(NORMAL, LIGHT);
	float intensity = calc_toonStripes(NdotL);
	DIFFUSE_LIGHT += ALBEDO*intensity*ATTENUATION*LIGHT_COLOR;
	if (useOutline) {
		float NdotV = dot(NORMAL, VIEW);
//		vec3 outline = mix(outline_color.rgb, vec3(1.0,1.0,1.0), NdotV);//step(outline_thickness, vec3(NdotV)));// - outline_thickness));
		vec3 outline = mix(outline_color.rgb, vec3(1.0,1.0,1.0), step(outline_thickness, vec3(NdotV)));// - outline_thickness));
		DIFFUSE_LIGHT *= outline * outline;
//		DIFFUSE_LIGHT = NORMAL;
	}
}